from utils.pre_process_tab import load_preprocess, find_cat_len
import pandas as pd
from models.gans import *
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy as np
from models.classifier import *
import time
import re
from utils.post_process import *
import random
from reprosyn.methods import MST
import json
import reprosyn
import os
from models.audit_classifier import *
from sklearn.preprocessing import OneHotEncoder, StandardScaler, KBinsDiscretizer
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
from sklearn.metrics import accuracy_score, precision_score, recall_score, precision_recall_curve, confusion_matrix
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.model_selection import train_test_split
from dp_analysis import *
plt.rcParams['text.usetex'] = True
from matplotlib.lines import Line2D


import warnings
from sklearn.exceptions import DataConversionWarning

warnings.filterwarnings(action='ignore', category=DataConversionWarning)

random.seed(42)
CONTINUOUS_COL=['age', 'capital_gain', 'capital_loss']
#TARGET= 'SeriousDlqin2yrs'
TARGET= 'over_50k'
LR= 0.0001 *5
ADULT_PTH= "/home/hadrien/data/adult/adult.csv"
DOMAIN_PTH= '/home/hadrien/data/adult/domain_adult.json'

def load_data(pth= ADULT_PTH):
    df= pd.read_csv(ADULT_PTH)
    df=df.drop(columns=['fnlwgt'])
    df = df.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '_', x))

    kbin= KBinsDiscretizer(n_bins=100, encode='ordinal', strategy='uniform')
    kbin.fit(df[CONTINUOUS_COL])
    df= pd.concat([df.drop(columns=CONTINUOUS_COL),
                    pd.DataFrame(kbin.transform(df[CONTINUOUS_COL]), columns= CONTINUOUS_COL)], axis=1)


    dataset_len= df.shape[0]
    df_f= df.iloc[:int(dataset_len/2),:]
    df_g= df.iloc[int(dataset_len/2):,:]
    df_gan= df.iloc[int(dataset_len/3):int(dataset_len/2),:]
    df_mia_tr= df.iloc[int(dataset_len/6):int(dataset_len/3),:]
    df_mia_te= df.iloc[:int(dataset_len/6),:]
    return df_f, df_g, df_gan, df_mia_tr, df_mia_te


def generate_synth(df, domain_pth= DOMAIN_PTH):
    # train reprosyn
    with open(domain_pth, 'r') as js_file:#reposyn read json with json.loads instead of json.load. this  workaround fix the metadata loading issues
            domaine= json.load(js_file)


    gen = MST(dataset=df.copy(), size=df.shape[0] *2, epsilon = 1000, metadata= domaine['columns'])
    #gen= 
    gen.run()
    synth= gen.output
    synth= synth[df.columns]
    return synth


# f and g pre-processing

def scale_col(train_x, test_x, sc_col):
    sc= StandardScaler()
    train_sc= train_x.drop(columns=[col for col in train_x.columns if col not in sc_col])
    test_sc= test_x.drop(columns=[col for col in test_x.columns if col not in sc_col])
    train_sc[sc_col]= sc.fit_transform(train_x[sc_col])
    test_sc[sc_col]= sc.transform(test_x[sc_col])
    return train_sc, test_sc, sc

def ohe_transform(train_x, test_x, ohe_col):
    ohe= OneHotEncoder(sparse=False, handle_unknown='ignore')
    ohe= ohe.fit(pd.concat([train_x, test_x], axis=0)[ohe_col])
    train_ohe= ohe.transform(train_x[ohe_col])
    test_ohe= ohe.transform(test_x[ohe_col])
    return pd.DataFrame(train_ohe, columns= ohe.get_feature_names()), pd.DataFrame(test_ohe, columns= ohe.get_feature_names()), ohe

def ml_preprocess(train_x, test_x, sc_col, ohe_col, return_transformers=False):
    train_sc, test_sc, sc= scale_col(train_x, test_x, sc_col)
    train_ohe, test_ohe, ohe= ohe_transform(train_x, test_x, ohe_col)
    train_x= pd.concat([train_sc.reset_index(drop=True), train_ohe.reset_index(drop=True)], axis=1)
    test_x= pd.concat([test_sc.reset_index(drop=True), test_ohe.reset_index(drop=True)], axis=1)
    if return_transformers:
        return train_x, test_x, sc, ohe
    else:
        return train_x, test_x

def ml_prepare(df, y_col, cat_col, num_col, ohe, sc):
    # TBD merge with ml_process
    df_x= df.drop(columns=[y_col])
    df_y= df[y_col]
    df_x_ohe= pd.DataFrame(ohe.transform(df_x[cat_col]), columns= ohe.get_feature_names())
    df_x_sc= df_x[num_col]
    df_x_sc[num_col]= sc.transform(df_x_sc)
    df_concat= pd.concat([df_x_sc.reset_index(drop=True), df_x_ohe.reset_index(drop=True)], axis=1)
    return pd.concat([df_concat.reset_index(drop=True), df_y.reset_index(drop=True)], axis=1)

class TabDataset(Dataset):
    def __init__(self, X, y):
        super().__init__()
        self.X= X.values
        self.y= y.values
    
    def __len__(self):
        return self.X.shape[0]
    
    def __getitem__(self, idx):
        return torch.Tensor(self.X[idx]), torch.Tensor([self.y[idx]])

def discriminateur(train_x, train_y, test_x, test_y, k, return_model=False):
    disc= LogReg(train_x, train_y, test_x)
    #disc= RandomForest(train_x, train_y, test_x, 100)
    #disc= GradientBoosting(train_x, train_y, test_x)
    pred= disc.train_predict()
    if return_model:
        return accuracy_score(test_y, np.where(pred>.5,1,0)), disc
    else:
        return accuracy_score(test_y, np.where(pred>.5,1,0))

def add_embedding(df, net, prefix, to_drop=['over_50k']):
    # may break with too big dataframe. would need to use dataloader
    if to_drop is not None:
        drop_df= df[to_drop]
        emb= net.extract_emb(torch.Tensor(df.drop(columns=to_drop).values)).cpu().detach().numpy()
    else:
        print('nothing to drop')
        emb= net.extract_emb(torch.Tensor(df.values)).cpu().detach().numpy()
    emb= pd.DataFrame(emb, columns=[prefix+'_emb_'+ str(i) for i in range(emb.shape[1])])
    return pd.concat([df.reset_index(drop=True), drop_df.reset_index(drop=True),emb], axis=1)

def add_loss(df, net, prefix,to_drop=['over_50k']):
    if to_drop is not None:
        with torch.no_grad():
            pred=net(torch.Tensor(df.drop(columns=to_drop).values))
            truth= torch.Tensor(df['over_50k'].values).reshape(-1,1)
            loss_fn= nn.BCELoss(reduction='none')
            loss= loss_fn(pred, truth)
            #loss= nn.BCELoss(net(torch.Tensor(df.drop(columns=to_drop).values)), torch.Tensor(df['over_50k'].values))
        df[prefix+'_loss']= loss.cpu().numpy()
    return df


#real_synth_x= add_embedding(real_synth_x, f, 'f')
#df_mia_tr= add_embedding(df_mia_tr, f, 'f')
#synth_tr= add_embedding(synth_tr, f, 'f')
#to_drop=['over_50k'] + [col for col in real_synth_x.columns if 'emb' in col]
#real_synth_x= add_embedding(real_synth_x, g, 'g', to_drop)
#df_mia_tr= add_embedding(df_mia_tr, g, 'g', to_drop)
#synth_tr= add_embedding(synth_tr, g, 'g', to_drop)


def remoove_obvious_synth(train_x, train_y, valid_x, valid_y):
    label= train_y.columns
    #disc= LogReg(train_x, train_y, test_x)
    #disc= RandomForest(train_x, train_y, test_x, 100)
    disc= GradientBoosting(train_x, train_y, valid_x)
    pred= disc.train_predict(30)
    valid = pd.concat([valid_x.reset_index(drop=True), valid_y.reset_index(drop=True), pd.DataFrame({'pred':pred})], axis=1)
    valid_keep= valid.loc[~((valid.member==0) & (valid.pred < .1)), valid.drop(columns=['pred']).columns]
    disc= GradientBoosting(valid_x, valid_y, train_x)
    pred= disc.train_predict(30)
    train = pd.concat([train_x.reset_index(drop=True), train_y.reset_index(drop=True), pd.DataFrame({'pred':pred})],axis=1)
    train_keep= train.loc[~((train.member==0)& (train.pred < .1)), valid.drop(columns=['pred']).columns]
    return train_keep.drop(columns=label), train_keep[label], valid_keep.drop(columns=label), valid_keep[label] 

def remoove_obvious_synth(quali_x, quali_y,train_x, train_y, valid_x, valid_y):
    label= train_y.columns
    #disc= LogReg(train_x, train_y, test_x)
    #disc= RandomForest(train_x, train_y, test_x, 100)
    disc= GradientBoosting(quali_x, quali_y, valid_x)
    pred= disc.train_predict(30)
    valid = pd.concat([valid_x.reset_index(drop=True), valid_y.reset_index(drop=True), pd.DataFrame({'pred':pred})], axis=1)
    valid_keep= valid.loc[~((valid.member==0) & (valid.pred < .1)), valid.drop(columns=['pred']).columns]
    pred= disc.best_gb.predict_proba(train_x)[:,1]
    train = pd.concat([train_x.reset_index(drop=True), train_y.reset_index(drop=True), pd.DataFrame({'pred':pred})],axis=1)
    train_keep= train.loc[~((train.member==0)& (train.pred < .1)), valid.drop(columns=['pred']).columns]
    return train_keep.drop(columns=label), train_keep[label], valid_keep.drop(columns=label), valid_keep[label] 
#self.best_gb.predict_proba(self.test_x)[:,1]


def balance_dataset(df_x, df_y):
    df = pd.concat([df_x, df_y], axis=1)
    real_df = df.loc[df.member == 0]
    synthetic_df = df.loc[df.member == 1]
    num_synthetic = len(synthetic_df)
    num_real = len(real_df)
    if num_real > num_synthetic:
        real_df = real_df.sample(n=num_synthetic, random_state=42)
    elif num_synthetic > num_real:
        synthetic_df = synthetic_df.sample(n=num_real, random_state=42)
    balanced_df = pd.concat([real_df, synthetic_df], axis=0).reset_index(drop=True)
    # Split the balanced dataframe back into X and y
    balanced_x = balanced_df.drop(columns=['member'])
    balanced_y = balanced_df[['member']]
    return balanced_x, balanced_y


def run_discriminator_loop(df_mia_tr, synth_tr, real_synth_x, real_synth_y, f_col, g_col, rng):
    vanilla, tab_f, tab_g=[],[],[]
    for k in rng:
        train_x= pd.concat([df_mia_tr.sample(k).reset_index(drop=True), synth_tr.sample(k).reset_index(drop=True)], axis=0)# retirer ca de la
        train_y= pd.DataFrame({'fake':[0 for i in range(k)]+[1 for i in range(k)]}) #label 1 for fake
        print('k=',k)
        vanilla.append(discriminateur(train_x.drop(columns=f_col+g_col), train_y, real_synth_x.drop(columns=f_col+g_col), real_synth_y,k))
        tab_f.append(discriminateur(train_x.drop(columns=g_col), train_y, real_synth_x.drop(columns=g_col), real_synth_y,k))
        tab_g.append(discriminateur(train_x.drop(columns=f_col), train_y, real_synth_x.drop(columns=f_col), real_synth_y,k))
    return vanilla, tab_f, tab_g

#f_emb_col= [col for col in real_synth_x.columns if 'f_emb' in col]
#g_emb_col= [col for col in real_synth_x.columns if 'g_emb' in col]


"""
PREVIOUS CODE WITH ACCURACY VS DATASET SIZE

rng= list(range(50,550,50))+list(range(500,5500,500))
vanilla_all, tab_f_all, tab_g_all= [],[],[]
for _ in range(10):
    print('round:', _)
    vanilla, tab_f, tab_g= run_discriminator_loop(df_mia_tr, synth_tr, real_synth_x, real_synth_y, f_emb_col, g_emb_col, rng)
    vanilla_all.append(vanilla)
    tab_f_all.append(tab_f)
    tab_g_all.append(tab_g)

vanilla_all=pd.DataFrame(vanilla_all)
tab_f_all=pd.DataFrame(tab_f_all)
tab_g_all=pd.DataFrame(tab_g_all)

vanilla_all.to_csv('vanilla.csv', index=False)
tab_f_all.to_csv('tab_f.csv', index=False)
tab_g_all.to_csv('tab_g.csv', index=False)


vanilla_mean, vanilla_std= vanilla_all.mean(), vanilla_all.std()
tab_f_mean, tab_f_std= tab_f_all.mean(), tab_f_all.std()
tab_g_mean, tab_g_std= tab_g_all.mean(), tab_g_all.std()

plt.figure()
plt.plot(rng, vanilla_mean, label='tab only', c='g')
plt.plot(rng, tab_f_mean, label='tab + f pred', c='r')
plt.plot(rng, tab_g_mean, label='tab + g pred', c='b')
plt.fill_between(rng, (vanilla_mean-vanilla_std), (vanilla_mean+vanilla_std), color='g', alpha=.2)
plt.fill_between(rng, (tab_f_mean-tab_f_std), (tab_f_mean+tab_f_std), color='r', alpha=.2)
plt.fill_between(rng, (tab_g_mean-tab_g_std), (tab_g_mean+tab_g_std), color='b', alpha=.2)
#plt.ylim(0,1)
plt.legend()
plt.show()
# validate synth quality


plt.figure()
plt.plot(rng[:10], vanilla_mean[:10], label='tab only', c='g')
plt.plot(rng[:10], tab_f_mean[:10], label='tab + f pred', c='r')
plt.plot(rng[:10], tab_g_mean[:10], label='tab + g pred', c='b')
plt.fill_between(rng[:10], (vanilla_mean[:10]-vanilla_std[:10]), (vanilla_mean[:10]+vanilla_std[:10]), color='g', alpha=.2)
plt.fill_between(rng[:10], (tab_f_mean[:10]-tab_f_std[:10]), (tab_f_mean[:10]+tab_f_std[:10]), color='r', alpha=.2)
plt.fill_between(rng[:10], (tab_g_mean[:10]-tab_g_std[:10]), (tab_g_mean[:10]+tab_g_std[:10]), color='b', alpha=.2)
#plt.ylim(0,1)
plt.legend()
plt.show()
# validate synth quality

"""

def roc_discriminateur(train_x, train_y, test_x, test_y):
    #disc= LogReg(train_x, train_y, test_x)
    #disc= RandomForest(train_x, train_y, test_x, 100)
    disc= GradientBoosting(train_x, train_y, test_x)
    pred= disc.train_predict()
    return roc_curve(test_y, pred)

def precision_discriminateur(train_x, train_y, test_x, test_y):
    #disc= LogReg(train_x, train_y, test_x)
    #disc= RandomForest(train_x, train_y, test_x, 100)
    disc= GradientBoosting(train_x, train_y, test_x)
    pred= disc.train_predict(max_evals=200)
    return [precision_score(test_y, np.where(pred> (i/100),1,0)) for i in range(100,-1,-1)], [recall_score(test_y, np.where(pred> (i/100),1,0)) for i in range(100,-1,-1)], [np.sum(pred> i/100) for i in range(100,-1,-1)], pred



#fpr_v, tpr_v, thresholds_v =roc_discriminateur(train_x.drop(columns=g_emb_col+f_emb_col), train_y, real_synth_x.drop(columns=g_emb_col+f_emb_col), real_synth_y.member)
#fpr_f, tpr_f, thresholds_f =roc_discriminateur(train_x.drop(columns=g_emb_col), train_y, real_synth_x.drop(columns=g_emb_col), real_synth_y.member)
#fpr_g, tpr_g, thresholds_g =roc_discriminateur(train_x.drop(columns=f_emb_col), train_y, real_synth_x.drop(columns=f_emb_col), real_synth_y.member)

#pred_g= np.load('audit_result/results_SATML/pred_g_final.npy')
#pred_f_base= np.load('audit_result/results_SATML/pred_f_base_final.npy')
#pred_f_overfit= np.load('audit_result/results_SATML/pred_f_overfit_final.npy')
#real_synth_y= np.load('audit_result/results_SATML/real_synth_y_final.npy')
#real_synth_y= pd.DataFrame({'member': real_synth_y.flatten()})

def privacy_recall(preds, labels):
    thresholds = np.linspace(0, 1, 1000)
    recalls = []
    eps_lbs = []
    for th in thresholds:
        hard_preds = (preds > th).astype('float')
        tn, fp, fn, tp = confusion_matrix(labels, hard_preds).ravel()
        if tp+fp == 0:
            continue
        eps_lb = get_eps_audit(len(preds), fp+tp, tp, 0., 0.05/2)
        recall = (tp) / (tp+fn)
        recalls.append(recall)
        eps_lbs.append(eps_lb)
    
    recalls = np.array(recalls)
    eps_lbs = np.array(eps_lbs)
    eps_max= np.max(eps_lbs)

    return thresholds, recalls, eps_lbs, eps_max

def privacy_do_plot(recall, privacy, max_privacy, **plot_kwargs):
    plt.plot(recall, privacy , plot_kwargs['color'], linewidth=plot_kwargs['linewidth'], label=plot_kwargs['legend'])
    plt.axhline(y=max_privacy, color=plot_kwargs['color'], linestyle='--')


def preision_recall_extract(preds, labels, eps_max):
    thresholds = np.linspace(0, 1, 1000) 
    recall_curve = []
    precision_curve = []
    theoretical_precision_curve = []
    for th in thresholds:
        hard_preds = (preds > th).astype('float')
        tn, fp, fn, tp = confusion_matrix(labels, hard_preds).ravel()
        if tp+fp == 0:
            continue
        recall = (tp) / (tp+fn)
        precision = (tp) / (tp+fp)
        recall_curve.append(recall)
        precision_curve.append(precision)
        r = tp + fp
        q = 1/(1+math.exp(-eps_max))
        theoretical_precision_curve.append(scipy.stats.binom.isf(0.05/2, r, q) / r)        
        #precision, recall, thresholds = precision_recall_curve(y_true=labels, probas_pred=preds)

    recall_curve, precision_curve, theoretical_precision_curve = np.array(recall_curve), np.array(precision_curve), np.array(theoretical_precision_curve)    
    return recall_curve, precision_curve, theoretical_precision_curve

def precision_do_plot(recall_mean, precision_mean, theoretical_precision_max, **plot_kwargs):
    confidence = 1.96
    plt.plot(recall_mean, precision_mean , plot_kwargs['color'], linewidth=plot_kwargs['linewidth'], label=plot_kwargs['legend'])
    plt.plot(recall_mean, theoretical_precision_max, plot_kwargs['color'], linewidth=plot_kwargs['linewidth'], linestyle='--')

def nb_pred_recall(preds, labels):
    thresholds = np.linspace(0, 1, 1000) 
    recall_curve = []
    nb_pred_curve = []
    tps=[]
    for th in thresholds:
        hard_preds = (preds > th).astype('float')
        tn, fp, fn, tp = confusion_matrix(labels, hard_preds).ravel()
        if tp+fp == 0:
            continue
        recall = (tp) / (tp+fn)
        nb_pred =  (tp+fp)
        recall_curve.append(recall)
        nb_pred_curve.append(nb_pred)
        tps.append(tp)
    return np.array(recall_curve), np.array(nb_pred_curve), np.array(tps)

def nb_pred_do_plot(recall, nb_pred, tps, **plot_kwargs):
    plt.plot(recall, nb_pred,plot_kwargs['color'], linewidth=plot_kwargs['linewidth'], label=plot_kwargs['legend'])
    if tps is not None:
        plt.plot(recall, tps,color='black', linewidth=plot_kwargs['linewidth'], label='Number of true positive',linestyle='--')

##############################################
# PANORAMIA MAIN                             #
##############################################


#def run_panoramia():
helper= 'synth'
exp_1=False #RMRN
exp_2=False #RFMFN
exp_3=False #RFMRFNM
retrain=True
full=False
O_1=False
dual_helper=True # we need helper=synth for this case

df_f, df_g, df_gan, df_mia_tr, df_mia_te= load_data()
if full:
    df_full= pd.concat([df_f, df_g], axis=0).reset_index(drop=True)
    df_f, df_g= train_test_split(df_full, test_size=.2)
    #df_mia_tr= pd.concat([df_mia_tr, df_gan], axis=0).reset_index(drop=True)
synth= generate_synth(df_f)
synth2= generate_synth(df_f) #used to filter bad quality synth data
if exp_2 or exp_3:
    synth3= generate_synth(df_f)
    synth3['over_50k']=synth3['over_50k'].astype('int64')


# exp 1 rm, rnm
if exp_1:
    df_g, df_rnm= train_test_split(df_g, test_size=.5, random_state=42)

# prepare data to train target and helper model
df_f_x= df_f.drop(columns=['over_50k'])
df_f_y= df_f['over_50k']
df_g_x= df_g.drop(columns=['over_50k'])
df_g_y= df_g['over_50k']

if exp_2 or exp_3:
    df_f_real_tr, df_f_real_te=train_test_split(df_f, test_size=.5, random_state=42)
    df_f_synth_tr= synth3.loc[synth3['native_country']!=' Holand-Netherlands'].sample(len(df_f_real_tr),  random_state=42)
    df_f= pd.concat([df_f_real_tr, df_f_synth_tr]).reset_index(drop=True)
    df_f_x= df_f.drop(columns=['over_50k'])
    df_f_x['education_num']= df_f_x['education_num'].astype('int64')
    df_f_y= df_f['over_50k'].astype('int64')

num_col=[col for col in df_f_x.columns if df_f_x[col].dtypes !='object']
cat_col= [col for col in df_f_x.columns if col not in num_col]
df_f_x, df_g_x, sc, ohe= ml_preprocess(df_f_x, df_g_x, num_col, cat_col, True)
synth2= ml_prepare(synth2,'over_50k', cat_col, num_col, ohe, sc)
synth2['over_50k']=synth2['over_50k'].astype('int64')
if exp_2 or exp_3:
    synth3= ml_prepare(synth3,'over_50k', cat_col, num_col, ohe, sc)
    df_f_real_tr= ml_prepare(df_f_real_tr,'over_50k', cat_col, num_col, ohe, sc)
    df_f_real_te= ml_prepare(df_f_real_te,'over_50k', cat_col, num_col, ohe, sc)
    df_f_synth_tr= ml_prepare(df_f_synth_tr,'over_50k', cat_col, num_col, ohe, sc)
f_dataset= TabDataset(df_f_x, df_f_y)
df_g= ml_prepare(df_g,'over_50k', cat_col, num_col, ohe, sc) # berk should be done with mlpreprocess

#synth_dataset= TabDataset(synth2.drop(columns=['over_50k']), synth2['over_50k'])
if helper=='real':
    helper_dataset= TabDataset(df_g_x, df_g_y)
elif helper=='synth':
    helper_dataset= TabDataset(synth2.drop(columns=['over_50k']), synth2['over_50k'])
    valid_dataset= TabDataset(df_g_x, df_g_y)
    valid_dataloader= DataLoader(valid_dataset, 256, False)
else:
    raise("error helper should be either real or synth")
f_dataloader= DataLoader(f_dataset, 512, True)#, num_workers=6)
helper_dataloader= DataLoader(helper_dataset, 64, True)#,  num_workers=6)

nb_var=df_f_x.shape[1]
#train f and g
criterion = nn.BCELoss()
f= DNN(nb_var)
optim_f= torch.optim.Adam(f.parameters(), lr= LR*20)
if retrain:
    if helper=='synth':
        train_network(f, 200, f_dataloader, valid_dataloader, optim_f, criterion, pth='models/f_refactor_full.pth')
    else:
        train_network(f, 100, f_dataloader, helper_dataloader, optim_f, criterion, pth='models/f_refactor_full.pth')
    torch.save(f, 'models/f_overfit_refactor_full.pth') #decomment to save overfitted model
    print('')



f_base= DNN(nb_var)
#optim_f= torch.optim.Adam(f.parameters(), lr= LR)
#train_network(f, 10, f_dataloader, helper_dataloader, optim_f, criterion, pth='models/f_base_refactor.pth')
#dp f model
#optimize_hyp_param(f_dataset, 30)
#train_network_dp(f, 20, f_dataloader, g_dataloader, optim_f, criterion, pth='models/f_dp_20.pth')
#torch.save(f, 'models/f_overfit_refactor.pth') #decomment to save overfitted model


g= DNN(nb_var)
optim_g= torch.optim.Adam(g.parameters(), lr= LR)
if retrain:
    train_network(g, 20, helper_dataloader, f_dataloader, optim_g, criterion, pth='models/g_synth_refactor_full.pth')


if dual_helper:
    assert helper=='synth', "helper should be synth"
    


f= torch.load('models/f_overfit_refactor_full.pth')
f_base= torch.load('models/f_refactor_full.pth')
g= torch.load('models/g_synth_refactor_full.pth')

# prepare data for panoramia
df_mia_tr= ml_prepare(df_mia_tr, 'over_50k', cat_col, num_col, ohe, sc)
df_mia_te= ml_prepare(df_mia_te, 'over_50k', cat_col, num_col, ohe, sc)
synth= ml_prepare(synth,'over_50k', cat_col, num_col, ohe, sc)
df_gan= ml_prepare(df_gan, 'over_50k', cat_col, num_col, ohe, sc)
if exp_1:
    df_rnm= ml_prepare(df_rnm, 'over_50k', cat_col, num_col, ohe, sc)

synth['over_50k']=synth['over_50k'].astype('int64')

synth_tr= synth.iloc[:len(df_mia_tr),:]
synth_te= synth.iloc[len(df_mia_tr):len(df_mia_tr)+len(df_mia_te),:]

real_synth_x= pd.concat([df_mia_te.reset_index(drop=True), synth_te.reset_index(drop=True)], axis=0)
real_synth_y= pd.DataFrame({'member':[1 for i in range(len(df_mia_te))]+[0 for i in range(len(synth_te))]})

train_x= pd.concat([df_mia_tr.reset_index(drop=True), synth_tr.reset_index(drop=True)], axis=0)
train_y= pd.DataFrame({'member':[1 for i in range(len(df_mia_tr))]+[0 for i in range(len(synth_tr))]})

# exp1 rm rnm
if exp_1:
    df_rnm_tr, df_rnm_te = train_test_split(df_rnm, test_size=0.5, random_state=42)
    df_rm_tr = df_mia_tr.sample(len(df_rnm_tr), random_state=42)
    df_rm_te = df_mia_te.sample(len(df_rnm_te), random_state=42)

    train_x = pd.concat([df_rm_tr.reset_index(drop=True), df_rnm_tr.reset_index(drop=True)], axis=0)
    train_y= pd.DataFrame({'member':[1 for i in range(len(df_rm_tr))]+[0 for i in range(len(df_rnm_tr))]})

    real_synth_x= pd.concat([df_rm_te.reset_index(drop=True), df_rnm_te.reset_index(drop=True)], axis=0)
    real_synth_y= pd.DataFrame({'member':[1 for i in range(len(df_rm_te))]+[0 for i in range(len(df_rnm_te))]})

elif exp_3:
    mem_real_tr= df_f_real_tr.iloc[:int(len(df_f_real_tr)/2),:]
    mem_synth_tr= df_f_synth_tr.iloc[:int(len(df_f_real_tr)/2),:]
    nmem_real_tr= df_g.iloc[:(int(len(df_f_real_tr)/2))]
    nmem_synth_tr= synth.iloc[:(int(len(df_f_real_tr)/2))]
    train_x= pd.concat([mem_real_tr,mem_synth_tr,nmem_real_tr,nmem_synth_tr])
    train_y= pd.DataFrame({'member':[1 for i in range(len(df_f_real_tr))]+[0 for i in range(len(df_f_real_tr))]})

    mem_real_te= df_f_real_tr.iloc[int(len(df_f_real_tr)/2):,:]
    mem_synth_te= df_f_synth_tr.iloc[int(len(df_f_real_tr)/2):,:]
    nmem_real_te= df_g.iloc[(int(len(df_f_real_tr)/2)):int(len(df_f_real_tr)),:]
    nmem_synth_te= synth.iloc[(int(len(df_f_real_tr)/2)):int(len(df_f_real_tr)),:]
    real_synth_x=pd.concat([mem_real_te,mem_synth_te,nmem_real_te,nmem_synth_te])
    real_synth_y= pd.DataFrame({'member':[1 for i in range(len(df_f_real_te))]+[0 for i in range(len(df_f_real_te))]})
    # remoove aubious bad synth data
    #df_real_syth_quali_x= pd.concat([synth2.reset_index(drop=True), df_gan.reset_index(drop=True)], axis=0).reset_index(drop=True)
    #df_real_syth_quali_y= pd.DataFrame({'member':[0 for i in range(len(synth2))]+[1 for i in range(len(df_gan))]})
    #train_x, train_y, real_synth_x, real_synth_y= remoove_obvious_synth(df_real_syth_quali_x,df_real_syth_quali_y,train_x, train_y, real_synth_x, real_synth_y)
    #train_x, train_y= balance_dataset(train_x, train_y)
    #real_synth_x, real_synth_y= balance_dataset(real_synth_x, real_synth_y)


else:
    if exp_2: # changer utiliser mix real synth for member
        #RFM FN
        df_mia_tr2 = df_f_real_tr.iloc[:int(len(df_f_real_tr)/4),:]
        df_mia_tr_synth= df_f_synth_tr.iloc[:int(len(df_f_real_tr)/4),:]
        df_mia_te2= df_f_real_tr.iloc[int(len(df_f_real_tr)/4):int(len(df_f_real_tr)/2),:]
        df_mia_te_synth= df_f_synth_tr.iloc[int(len(df_f_real_tr)/4):int(len(df_f_real_tr)/2),:]
        train_x= pd.concat([df_mia_tr2,df_mia_tr_synth, synth.iloc[:(len(df_mia_tr2)+len(df_mia_tr_synth)),:]], axis=0)
        train_y= pd.DataFrame({'member':[1 for i in range(len(df_mia_tr2)+len(df_mia_tr_synth))]+[0 for i in range(len(df_mia_tr2)+len(df_mia_tr_synth))]})
        real_synth_x= pd.concat([df_mia_te2, df_mia_te_synth,synth.iloc[(len(df_mia_tr2)+len(df_mia_tr_synth)):(len(df_mia_tr2)+len(df_mia_tr_synth)+len(df_mia_te2)+len(df_mia_te_synth)),:]], axis=0)
        real_synth_y= pd.DataFrame({'member':[1 for i in range(len(df_mia_te2)+len(df_mia_te_synth))]+[0 for i in range(len(df_mia_te2)+len(df_mia_te_synth))]})
    # remoove aubious bad synth data
    if full:
        df_real_syth_quali_x= pd.concat([synth2.reset_index(drop=True).sample(len(df_g)*2), df_g.reset_index(drop=True)], axis=0).reset_index(drop=True)
        df_real_syth_quali_y= pd.DataFrame({'member':[0 for i in range(len(df_g)*2)]+[1 for i in range(len(df_g))]})
    else:
        df_real_syth_quali_x= pd.concat([synth2.reset_index(drop=True), df_gan.reset_index(drop=True)], axis=0).reset_index(drop=True)
        df_real_syth_quali_y= pd.DataFrame({'member':[0 for i in range(len(synth2))]+[1 for i in range(len(df_gan))]})
    train_x, train_y, real_synth_x, real_synth_y= remoove_obvious_synth(df_real_syth_quali_x,df_real_syth_quali_y,train_x, train_y, real_synth_x, real_synth_y)


    train_x, train_y= balance_dataset(train_x, train_y)
    real_synth_x, real_synth_y= balance_dataset(real_synth_x, real_synth_y)


# ADD loss from helper and target model
real_synth_x= add_loss(real_synth_x, f, 'f')
train_x= add_loss(train_x, f, 'f')
to_drop=['over_50k'] + [col for col in real_synth_x.columns if 'f_loss' in col]
real_synth_x= add_loss(real_synth_x, f_base, 'f2', to_drop)
train_x= add_loss(train_x, f_base, 'f2', to_drop)
to_drop=['over_50k'] +[col for col in real_synth_x.columns if 'f_loss' in col or 'f2_loss' in col]
real_synth_x= add_loss(real_synth_x, g, 'g', to_drop)
train_x= add_loss(train_x, g, 'g', to_drop)

f_emb_col= [col for col in real_synth_x.columns if 'f_loss' in col]
f2_emb_col= [col for col in real_synth_x.columns if 'f2_loss' in col]
g_emb_col= [col for col in real_synth_x.columns if 'g_loss' in col]

from audit_O1 import *
if O_1:
    r, v= find_O1_pred(pd.concat([real_synth_x.reset_index(drop=True), real_synth_y.reset_index(drop=True)], axis=1).rename(columns={'f_loss':'loss'}))
    eps_O1_overfit= get_eps_audit(len(real_synth_x),r,v, 0., 0.05/2)
    print('EPS O(1) overfit:', eps_O1_overfit)
    r, v= find_O1_pred(pd.concat([real_synth_x.reset_index(drop=True), real_synth_y.reset_index(drop=True)], axis=1).rename(columns={'f2_loss':'loss'}))
    eps_O1_base= get_eps_audit(len(real_synth_x),r,v, 0., 0.05/2)
    print('EPS O(1) base:', eps_O1_base)

precision_g, recall_g, n_g, pred_g= precision_discriminateur(train_x.drop(columns=f_emb_col+f2_emb_col), train_y, real_synth_x.drop(columns=f_emb_col+f2_emb_col), real_synth_y.member)
precision_f_overfit, recall_f_overfit, n_f_overfit, pred_f_overfit= precision_discriminateur(train_x.drop(columns=g_emb_col+ f2_emb_col), train_y, real_synth_x.drop(columns=g_emb_col+ f2_emb_col), real_synth_y.member)
precision_f_base, recall_f_base, n_f_base, pred_f_base= precision_discriminateur(train_x.drop(columns=g_emb_col + f_emb_col), train_y, real_synth_x.drop(columns=g_emb_col+ f_emb_col), real_synth_y.member)


th, recall_g, eps_g, eps_max_g= privacy_recall(pred_g, real_synth_y.member)
print(eps_max_g)
th, recall_f_base, eps_f_base, eps_max_f_base= privacy_recall(pred_f_base, real_synth_y.member)
th, recall_f_overfit, eps_f_overfit, eps_max_f_overfit= privacy_recall(pred_f_overfit, real_synth_y.member)




plt.figure(figsize=(10,8))
privacy_do_plot(recall_g, eps_g, eps_max_g, color='b',legend=r'$c_\textrm{lb}$',linewidth=2.5)
privacy_do_plot(recall_f_overfit, eps_f_overfit, eps_max_f_overfit, color='r',legend=r"$\{{c+\epsilon}\}_{\textrm{lb}}$ MLP_E100",linewidth=2.5)
privacy_do_plot(recall_f_base, eps_f_base, eps_max_f_base, color='m',legend=r"$\{{c+\epsilon}\}_{\textrm{lb}}$ MLP_E10",linewidth=2.5)

plt.xticks(fontsize=24)
plt.yticks(fontsize=24)
plt.tick_params(axis='both', which='both', length=5)
plt.xlabel('Recall', fontsize=32)
plt.ylabel('Empirical value', fontsize=32)
plt.tight_layout()
# Get handles and labels from the existing legend
handles, labels = plt.gca().get_legend_handles_labels()
# Add custom legend entry
custom_line = Line2D([0], [0], color='black', linestyle='--', label='empirical maximum value')
handles.append(custom_line)
labels.append(custom_line.get_label())
# Create a single legend with all entries
plt.legend(handles=handles, labels=labels, fontsize=28, loc='upper right')
#plt.savefig('audit_result/results_ICML/tab_privacy_recall_full.pdf', format='pdf')
#plt.close()
plt.show()

recall_g, precision_g, th_precision_g = preision_recall_extract(pred_g, real_synth_y.member, eps_max_g)
recall_f_base, precision_f_base, th_precision_f_base = preision_recall_extract(pred_f_base, real_synth_y.member, eps_max_f_base)
recall_f_overfit, precision_f_overfit, th_precision_f_overfit = preision_recall_extract(pred_f_overfit, real_synth_y.member, eps_max_f_overfit)

plt.figure(figsize=(10,8))
precision_do_plot(recall_g, precision_g, th_precision_g,color='b',legend=f"baseline model",alpha=0.09,linewidth=2.5)
precision_do_plot(recall_f_overfit, precision_f_overfit, th_precision_f_overfit,color='r',legend=f"PANORAMIA MLP_E100",alpha=0.09,linewidth=2.5)
precision_do_plot(recall_f_base, precision_f_base, th_precision_f_base,color='m',legend=f"PANORAMIA MLP_E10",alpha=0.09,linewidth=2.5)
plt.xticks(fontsize=24)
plt.yticks(fontsize=24)
plt.tick_params(axis='both', which='both', length=5)
plt.xlabel('Recall', fontsize=32)
plt.ylabel('Precision', fontsize=32)
plt.tight_layout()
# Get handles and labels from the existing legend
handles, labels = plt.gca().get_legend_handles_labels()
# Add custom legend entry
custom_line = Line2D([0], [0], color='black', linestyle='--', label='Theoretical maximum precision')
handles.append(custom_line)
labels.append(custom_line.get_label())
# Create a single legend with all entries
plt.legend(handles=handles, labels=labels, fontsize=28, loc='lower left')
plt.legend(fontsize=18, loc='lower left')
#plt.savefig('audit_result/results_ICML/tab_precision_recall_full.pdf', format='pdf')
#plt.close()
plt.show()


recall_g, nb_pred_g, tps_g= nb_pred_recall(pred_g, real_synth_y.member)
recall_f_base, nb_pred_f_base, tps_base= nb_pred_recall(pred_f_base, real_synth_y.member)
recall_f_overfit, nb_pred_f_overfit, tps= nb_pred_recall(pred_f_overfit, real_synth_y.member)

plt.figure(figsize=(10,8))
nb_pred_do_plot(recall_g, nb_pred_g, None, color='b',legend=f"Baseline ",alpha=0.09,linewidth=2.5)
nb_pred_do_plot(recall_f_overfit, nb_pred_f_overfit, None, color='r',legend=f"PANORAMIA MLP_E100",alpha=0.09,linewidth=2.5)
nb_pred_do_plot(recall_f_base, nb_pred_f_base, tps_base, color='m',legend=f"PANORAMIA MLP_E10",alpha=0.09,linewidth=2.5)
plt.xticks(fontsize=24)
plt.yticks(fontsize=24)
plt.tick_params(axis='both', which='both', length=5)
plt.xlabel('Recall', fontsize=32)
plt.ylabel('Number of Predictions', fontsize=32)
plt.tight_layout()
plt.legend(fontsize=28, loc='upper left')
#plt.savefig('audit_result/results_ICML/tab_nbpred_recall_full.pdf', format='pdf')
#plt.close()
plt.show()


#np.save('audit_result/results_ICML/pred_g.npy', pred_g)
#np.save('audit_result/results_ICML/pred_f_base.npy', pred_f_base)
#np.save('audit_result/results_ICML/pred_f_overfit.npy', pred_f_overfit)
#np.save('audit_result/results_ICML/real_synth_y.npy', real_synth_y)
#if __name__=="__main__":
#run_panoramia()

"""

pr_g, rec_g, _= precision_recall_curve(real_synth_y.member, pred_g)
pr_f_base, rec_f_base, _= precision_recall_curve(real_synth_y.member, pred_f_base)
pr_f_overfit, rec_f_overfit, _= precision_recall_curve(real_synth_y.member, pred_f_overfit)

plt.plot(rec_g, pr_g, label= 'baseline', color='blue')
plt.plot(rec_f_base, pr_f_base, label= 'mia', color='pink')
plt.plot(rec_f_overfit, pr_f_overfit, label='mia overfit', color='red')
plt.xlabel('recall')
plt.ylabel('precision')
plt.legend()
plt.show()



#plt.figure()
#plt.plot(recall_g, precision_g, label='baseline', color='blue')
#plt.plot(recall_f_base, precision_f_base, label='f', color='pink')
#plt.plot(recall_f_overfit, precision_f_overfit, label='f overfit', color='red')
#plt.xscale('log')  # set x-axis to log scale as per your range
#plt.yscale('log')  # set x-axis to log scale as per your range
#plt.xlim([10**-2, 10**0])
#plt.ylim([10**-2, 10**0])
#plt.legend()
#plt.show()

g_audit= [get_eps_audit(n_g[i], n_g[i], int(n_g[i]* precision_g[i]), 0.,0.05/2) for i in range(len(n_g))]
#print(f'fake data are {g_audit} good')

eps_g_audit_base= [get_eps_audit(n_f_base[i], n_f_base[i], int(n_f_base[i]* precision_f_base[i]), 0.,0.05/2) for i in range(len(n_f_base))]
eps_g_audit_overfit= [get_eps_audit(n_f_overfit[i], n_f_overfit[i], int(n_f_overfit[i]* precision_f_overfit[i]), 0.,0.05/2) for i in range(len(n_f_overfit))]
#eps= eps_g_audit-g_audit
#print(f'epds +g = {eps_g_audit}, eps= {eps}')
eps_base= np.array(eps_g_audit_base)- np.array(g_audit)
eps_overfit= np.array(eps_g_audit_overfit)- np.array(g_audit)


plt.plot(np.array(range(100,-1,-1))/100, g_audit, label='c', color='blue')
plt.plot(np.array(range(100,-1,-1))/100, eps_g_audit_base, label='c+eps', color='lightgreen')
#plt.plot(np.array(range(100,-1,-1))/100, eps_base, label='eps', color='pink')
plt.plot(np.array(range(100,-1,-1))/100, eps_g_audit_overfit, label='c+eps overfit', color='darkgreen')
#plt.plot(np.array(range(100,-1,-1))/100, eps_overfit, label='eps overfit', color='red')

plt.xlabel('threshold')
plt.legend()
plt.gca().invert_xaxis()
plt.show()


eps_overfit[10]

get_eps_audit_simple(int(n_f_overfit[10]* precision_f_overfit[10]), n_f_overfit[10], 0.05)


# Create plot
plt.figure()
plt.plot(fpr_v, tpr_v, label='tab only')
plt.plot(fpr_f, tpr_f, label='tab+ f loss')
plt.plot(fpr_g, tpr_g, label='tab+ g loss')

# This line represents random guessing.
plt.plot([0,1], [0, 1], color='navy', linestyle='--', label='Random guessing')
plt.xlim([10**-5, 10**0])
plt.ylim([10**-5, 10**0])
plt.xscale('log')  # set x-axis to log scale as per your range
plt.yscale('log')  # set x-axis to log scale as per your range
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic')
plt.legend(loc="lower right")
plt.show()

import pickle

roc_v= (fpr_v, tpr_v, thresholds_v)
roc_f= (fpr_f, tpr_f, thresholds_f)
roc_g= (fpr_g, tpr_g, thresholds_g)

with open('audit_result/roc_overfit_v_dp10_1.pkl', 'wb') as f:
    pickle.dump(roc_v, f)

with open('audit_result/roc_overfit_f_dp10_1.pkl', 'wb') as f:
    pickle.dump(roc_f, f)

with open('audit_result/roc_overfit_g_dp10_1.pkl', 'wb') as f:
    pickle.dump(roc_g, f)

with open('audit_result/roc_overfit_v_dp10_1.pkl', 'rb') as f:
    roc_v = pickle.load(f)
with open('audit_result/roc_overfit_f_dp10_1.pkl', 'rb') as f:
    roc_f =pickle.load(f)
with open('audit_result/roc_overfit_g_dp10_1.pkl', 'rb') as f:
    roc_g =pickle.load(f)

plt.figure()
plt.plot(roc_v[0], roc_v[1], label='tab only')
plt.plot(roc_f[0], roc_f[1], label='tab+ f loss')
plt.plot(roc_g[0], roc_g[1], label='tab+ g loss')

# This line represents random guessing.
plt.plot([0,1], [0, 1], color='navy', linestyle='--', label='Random guessing')
plt.xlim([10**-4, 10**0])
plt.ylim([10**-4, 10**0])
plt.xscale('log')  # set x-axis to log scale as per your range
plt.yscale('log')  # set x-axis to log scale as per your range
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic')
plt.legend(loc="lower right")
plt.show()

fpr_v_all=[]
tpr_v_all=[]
threshold_v_all=[]
# Lists to store all fpr and tpr values for the 5 experiments
fpr_v_all = []
tpr_v_all = []

fpr_f_all = []
tpr_f_all = []
threshold_f_all=[]

fpr_g_all = []
tpr_g_all = []
threshold_g_all=[]

fpr_lira_all = []
tpr_lira_all = []
threshold_lira_all=[]


for i in range(1,6):
    # Loading data for model v
    with open(f'audit_result/roc_overfit_v_dp5_{i}.pkl', 'rb') as f:
        roc_v = pickle.load(f)
        fpr_v, tpr_v, thresholds_v = roc_v
        fpr_v_all.append(fpr_v)
        tpr_v_all.append(tpr_v)
        threshold_v_all.append(thresholds_v)
        
    # Loading data for model f
    with open(f'audit_result/roc_overfit_f_dp5_{i}.pkl', 'rb') as f:
        roc_f = pickle.load(f)
        fpr_f, tpr_f, thresholds_f = roc_f
        fpr_f_all.append(fpr_f)
        tpr_f_all.append(tpr_f)
        threshold_f_all.append(thresholds_f)

        
    # Loading data for model g
    with open(f'audit_result/roc_overfit_g_dp5_{i}.pkl', 'rb') as f:
        roc_g = pickle.load(f)
        fpr_g, tpr_g, thresholds_g = roc_g
        fpr_g_all.append(fpr_g)
        tpr_g_all.append(tpr_g)
        threshold_g_all.append(thresholds_g)
    
    #with open(f'/home/hadrien/Documents/Phd/LIRA/lira/audit_result/roc_lira_boost_overfit_{i}.pkl', 'rb') as f:
    #    roc_lira = pickle.load(f)
    #    fpr_lira, tpr_lira, thresholds_lira = roc_lira
    #    fpr_lira_all.append(fpr_lira)
    #    tpr_lira_all.append(tpr_lira)
    #    #threshold_g_all.append(thresholds_g)


def interpolate_tpr(tprs, fprs):
    tprs_interpol= []
    max_len= np.max([len(i) for i in tprs])
    ls= np.linspace(0,1,max_len)
    for tpr, fpr in zip(tprs, fprs):
        tpr_interpol= np.interp(ls, fpr, tpr)
        tprs_interpol.append(tpr_interpol)
    return tprs_interpol, ls

def plot_with_confidence_interval(fprs, tprs, color, label_name):
    tprs_interpol, ls= interpolate_tpr(tprs, fprs)
    # Calculate mean and std dev
    mean_tpr = pd.DataFrame(tprs_interpol).mean()#np.mean(tprs, axis=0)
    std_tpr = pd.DataFrame(tprs_interpol).std()#np.std(tprs, axis=0)

    # Calculate 95% confidence interval
    ci_upper = mean_tpr + 1.96 * (std_tpr/np.sqrt(5))
    ci_lower = mean_tpr - 1.96 * (std_tpr/np.sqrt(5))

    # Plotting
    plt.plot(ls, mean_tpr, color=color, label=label_name)
    plt.fill_between(ls, ci_lower, ci_upper, color=color, alpha=0.2)

# Sample data: 5 experiments for each
plt.figure()

# Plot each set of curves with their confidence intervals
plot_with_confidence_interval(fpr_v_all, tpr_v_all, 'blue', 'tab only')
plot_with_confidence_interval(fpr_f_all, tpr_f_all, 'green', 'tab + f loss')
plot_with_confidence_interval(fpr_g_all, tpr_g_all, 'red', 'tab + g loss')
#plot_with_confidence_interval(fpr_lira_all, tpr_lira_all, 'grey', 'LIRA')

# Rest of your plot details
plt.plot([0,1], [0, 1], color='navy', linestyle='--', label='Random guessing')
plt.xlim([10**-4, 10**0])
plt.ylim([10**-4, 10**0])
plt.xscale('log')
plt.yscale('log')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC Curve with Confidence Intervals')
plt.legend(loc="lower right")
plt.show()

tpr_v_inter_all, fpr_v_inter_all= interpolate_tpr(tpr_v_all, fpr_v_all)
tpr_f_inter_all, fpr_f_inter_all= interpolate_tpr(tpr_f_all, fpr_f_all)
tpr_g_inter_all, fpr_g_inter_all= interpolate_tpr(tpr_g_all, fpr_g_all)
tpr_lira_inter_all, fpr_lira_inter_all= interpolate_tpr(tpr_lira_all, fpr_lira_all)


for i in range(5):
    roc_v= pd.DataFrame({'base_fpr': fpr_v_inter_all, 'base_tpr':tpr_v_inter_all[i]})
    roc_v['base_roc_auc']= auc(fpr_v_inter_all,tpr_v_inter_all[i])
    roc_v.to_csv(f'audit_result/roc_noloss_overfit_dp5_{i}.csv')

    roc_f= pd.DataFrame({'mia_fpr': fpr_f_inter_all, 'mia_tpr':tpr_f_inter_all[i]})
    roc_f['mia_roc_auc']= auc(fpr_f_inter_all,tpr_f_inter_all[i])
    roc_f.to_csv(f'audit_result/roc_mia_overfit_dp5_{i}.csv')

    roc_g= pd.DataFrame({'base_fpr': fpr_g_inter_all, 'base_tpr':tpr_g_inter_all[i]})
    roc_g['base_roc_auc']= auc(fpr_g_inter_all,tpr_g_inter_all[i])
    roc_g.to_csv(f'audit_result/roc_base_overfit_dp5_{i}.csv')

    roc_lira= pd.DataFrame({'lira_fpr': fpr_lira_inter_all, 'lira_tpr':tpr_lira_inter_all[i]})
    roc_lira['base_roc_auc']= auc(fpr_lira_inter_all,tpr_lira_inter_all[i])
    roc_lira.to_csv(f'audit_result/roc_lira_overfit_{i}.csv')
"""
