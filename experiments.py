from utils.pre_process_tab import load_preprocess, find_cat_len
import pandas as pd
from models.gans import *
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy as np
from models.classifier import *
from attacks.distance_membership import *
from attacks.shadow_membership import *
from attacks.collision_membership import *
import time
from utils.viz import vizualize_membership
import re
from utils.post_process import *

TARGET= 'SeriousDlqin2yrs'
#TARGET= 'over_50k'


train_x, test_x, train_y, test_y= load_preprocess("D:/data/adult_5k.csv", ['fnlwgt'], TARGET)

real= pd.concat([train_x, train_y], axis=1)
real= real.dropna()
real=real[(np.abs(real)<=25).all(axis=1)]
#real= real.round()
real = real.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))


#real, test_membership= train_test_split(real, train_size=0.8, random_state=42)
test_membership= pd.concat([test_x, test_y], axis=1)
test_membership = test_membership.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

#end=test_membership.shape[0]%64
#test_membership= test_membership.iloc[:-end,:] # dirty but mini batch discrimination need batch of exactly 64

real_x= real.drop(columns=[TARGET])
real_y= real[TARGET]

ohe_dim= list(find_cat_len(real))
#ohe_dim.append(1) # pour ajouter la target aux var cat

continuous_dim= len(real.columns)- sum(ohe_dim) -1 # -1 pour la colonne target

#cgan= CGan(32,11,0.00005)
#cgan.fit(real.values, 64, 100)
gan= WCGan(64, real_x.shape[1], 0.000001,1)
#gan= WCGanDP(64, real_x.shape[1], 0.000001, 5., 1/real.shape[0], 1.,1)
#gan= ACGanCat(64, continuous_dim, ohe_dim, 0.000001, 3)
#gan= IACGan(64,107,.00001, 4)
#gan= IGan(64,11,.000005, 1)

t= time.time()
gan.fit(real_x.values, real_y.values, 128, 750)
print('training took', time.time()-t)

#gan.fit(real.values, 64, 500)

plt.figure(figsize=(10,5))
plt.title("Generator and Discriminator Loss During Training")
plt.plot(gan.G_losses,label="G")
plt.plot(gan.D_losses,label="D")
plt.xlabel("iterations")
plt.ylabel("Loss")
plt.legend()
plt.show()

fake= gan.generate_fake(real_y.values)
#fake= gan.generate_fake(real.shape[0])

fake= pd.DataFrame(fake.cpu().detach().numpy(), columns= real_x.columns)
#fake= pd.DataFrame(fake.detach().cpu().numpy(), columns= real.columns)
#int_col= real_x.columns.tolist()[:continuous_dim]
int_col= ['age', 'NumberOfTime3059DaysPastDueNotWorse', 'NumberOfOpenCreditLinesAndLoans', 'NumberOfTimes90DaysLate', 'NumberRealEstateLoansOrLines', 'NumberOfTime6089DaysPastDueNotWorse', 'NumberOfDependents']
for col in int_col:
  fake[col]= transform_to_ref_values(fake, real, col, col)
#fake= fake.round()
fake= pd.concat([fake, real_y.reset_index(drop=True)], axis=1)


#test_x= test_membership.drop(columns=['SeriousDlqin2yrs'])
#test_y= test_membership['SeriousDlqin2yrs']

#test_x_synth= gan.generate_fake(test_y.values)
#test_x_synth= pd.DataFrame(test_x_synth.detach().numpy(), columns= test_x.columns)

#plt.figure
#plt.hist(real_x.age, label='real', bins=100, alpha=0.5)
#plt.hist(fake.age, label='fake', bins=100, alpha= 0.5)
#plt.legend()
#plt.plot()

#real_y_mc= real_y.sample(n=1000000, replace=True)
#fake_mc= gan.generate_fake(real_y_mc.values)
#fake_mc= pd.DataFrame(fake_mc.detach().numpy(), columns= real_x.columns)

fake= pd.concat([fake, real_y.reset_index(drop=True)], axis=1)


for col in real.columns:
#for col in real.columns[:continuous_dim]:
  fake[col]= transform_to_ref_values(fake, real, col, col)



cols= fake.columns
fig, ax= plt.subplots(int(len(real.columns)/2)+1,2, figsize=(10,120))
z=0   
for i in range(int(len(real.columns)/2)+1):
  for j in range(2):
    ax[i,j].hist(real[cols[z]], label= 'real', bins=100, cumulative=True, histtype='step')
    ax[i,j].hist(fake[cols[z]], label= 'fake', bins=100, cumulative=True, histtype='step')
    ax[i,j].set_title(cols[z])
    z+=1
    if z>= len(cols)-1:
        break

plt.legend()
plt.show()

real_ml= real.sample(frac=.1)
fake_ml= fake.sample(frac=.1)

mc= ModelComparaison(real.drop(TARGET, axis=1), real[TARGET], test_x, test_y, fake.drop(TARGET, axis=1), fake[TARGET])
#mc= ModelComparaison(real_x, real_y, test_x, test_y, fake.drop('over_50k', axis=1), fake['over_50k'])
mc()


def print_membership_results(test):
  print('confusion matrix:',confusion_matrix(test.is_member, test.pred))
  print('precision',precision_score(test.is_member, test.pred))
  print('recall',recall_score(test.is_member, test.pred))
  print('f1:',f1_score(test.is_member, test.pred))
  print('accuracy', accuracy_score(test.is_member, test.pred))

print('\nGAN Leak')
test=gan_leak_attack(real.sample(n= test_membership.shape[0], random_state=42),test_membership, fake,.5)
print_membership_results(test)
threshold= test.score.quantile(.01)
test['pred']= np.where(test.score<= threshold,1,0)
print('############### 1%')
print_membership_results(test)
#vizualize_membership(test, 'tsne', 2)
print('\n MC attack')
test_mc=monte_carlo_attack(real.sample(n= test_membership.shape[0], random_state=42),test_membership, fake,.5)
print_membership_results(test_mc)
threshold= test_mc.score.quantile(.99)
test_mc['pred']= np.where(test_mc.score>= threshold,1,0)
print('############### 1%')
print_membership_results(test_mc)
#vizualize_membership(test, 'tsne', 2)
print('\nLOGAN')
test_logan= logan_attack_c(real.sample(n= test_membership.shape[0], random_state=42),test_membership, fake,.5,TARGET, 750)
print_membership_results(test_logan)
threshold= test_logan.score.quantile(.01)
test_logan['pred']= np.where(test_logan.score<= threshold,1,0)
print('############### 1%')
print_membership_results(test_logan)
#vizualize_membership(test, 'tsne', 2)
print('\nTable Gan')
test_table= table_gan_attack_c(real.sample(n= test_membership.shape[0], random_state=42),test_membership, fake,.5, TARGET, 750)
print_membership_results(test_table)
threshold= test_table.score.quantile(.99)
test_table['pred']= np.where(test_table.score>= threshold,1,0)
print('############### 1%')
print_membership_results(test_table)
#vizualize_membership(test, 'tsne', 2)

print('collision attack')
#fake=fake.round()
#int_col= real.columns[:continuous_dim].tolist()+ [TARGET]
#synth, rf=collision_attack_wc(real, fake, TARGET, 200, real.columns[:continuous_dim])
#precision, recall, th= precision_recall_curve(synth.collision, synth.score)
#plt.figure()
#plt.plot(recall, precision, label= 'real')
#plt.title('Gradient Boosting')
#plt.xlabel('recall')
#plt.ylabel('precision') 
#plt.show()

#print('f1:', f1_score(synth.collision, np.where(synth.score>=.5,1,0)))
#print('precision:', precision_score(synth.collision, np.where(synth.score>=.5,1,0)))
#print('recall:', recall_score(synth.collision, np.where(synth.score>=.5,1,0)))

#from functools import reduce
#df_list=[]
#for df in [test, test_mc, test_logan, test_table]:
#  reid= df.loc[(df.is_member==1)& (df.is_member== df.pred),[col for col in df.columns if col not in ['is_member', 'pred', 'score']]]
#  df_list.append(reid)
#reid= reduce(lambda a,b: pd.concat([a,b], axis=0, ignore_index=True), df_list)
#print(reid.shape)
#print(reid.drop_duplicates().shape)
#fake.to_csv('/home/hadrien/data/kaggle/giveme/fake_IACGAN.csv', index=False)



# sorted(zip(clf.feature_importances_, X.columns), reverse=True)
#feature_imp = pd.DataFrame(sorted(zip(rf.best_rf.feature_importances_,synth.drop(columns=['score', 'collision']).columns)), columns=['Value','Feature'])
#plt.figure()
#sns.barplot(x="Value", y="Feature", data=feature_imp.sort_values(by="Value", ascending=False))
#plt.title('Classifier feature imp')
#plt.show()

#fake.to_csv('/home/hadrien/data/kaggle/giveme/fake_WCGAN.csv', index=False)

#fake= pd.read_csv('/home/hadrien/data/kaggle/giveme/fake_ICGAN.csv')
#fake= pd.concat([fake, real_y.reset_index(drop=True)], axis=1)
#fake = fake.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))
