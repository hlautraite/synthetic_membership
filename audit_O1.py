import pandas as pd
import numpy as np

def find_O1_pred(pred_df):
    """
    pred_df  pandas DataFrame containing one column with the loss called loss and one column with membership called member (0: NM, 1:M)
    """
    threshold_range = np.arange(np.min(pred_df['loss']), np.max(pred_df['loss'])+ 0.01, 0.01)

    # Step 1: Find t_pos that maximizes precision for positive predictions
    best_precision = 0
    best_t_pos = 0
    for t_pos in threshold_range:
        positive_predictions = pred_df.loc[pred_df['loss'] <= t_pos]
        if len(positive_predictions) == 0:
            continue

        true_positives = sum(positive_predictions['member'] == 1)
        precision = true_positives / len(positive_predictions)
        if precision > best_precision:
            best_precision = precision
            best_t_pos = t_pos

    # Step 2: With t_pos fixed, find t_neg that maximizes overall accuracy
    best_accuracy = 0
    best_t_neg = 0
    total_predictions = 0
    correct_predictions = 0

    for t_neg in reversed(threshold_range):
        if t_neg <= best_t_pos:
            break

        # Split confident predictions into positive and negative predictions
        positive_predictions = pred_df.loc[(pred_df['loss'] <= best_t_pos)]
        negative_predictions = pred_df.loc[pred_df['loss'] >= t_neg]

        # Calculate correct predictions for positive and negative separately
        correct_positives = sum(positive_predictions['member'] == 1)
        correct_negatives = sum(negative_predictions['member'] == 0)

        # Total number of confident predictions
        r = len(positive_predictions) + len(negative_predictions)
        v = correct_positives + correct_negatives

        if r > 0:
            accuracy = v / r
            if accuracy > best_accuracy:
                best_accuracy = accuracy
                best_t_neg = t_neg
                total_predictions = r
                correct_predictions = v

    print(f"Best precision for t_pos: {best_precision} with t_pos: {best_t_pos}")
    print(f"Best accuracy: {best_accuracy} with thresholds (t_neg, t_pos): ({best_t_neg}, {best_t_pos})")
    return total_predictions, correct_predictions

