from turtle import forward
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data_utils
from torch.autograd import Variable
from torch.nn.parameter import Parameter
import torch.nn.functional as F
from opacus import PrivacyEngine
from opacus.utils.batch_memory_manager import BatchMemoryManager

DISC_MUL=3


device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')  # Slower with cpu 
#device = torch.device('cpu')
print(device)

def weights_init(m):
    if type(m) == nn.Linear:
        torch.nn.init.xavier_uniform_(m.weight)
        m.bias.data.fill_(0.01)

class MiniBatchDiscrimination(nn.Module):
	def __init__(self, A, B, C, batch_size):
		super(MiniBatchDiscrimination, self).__init__()
		self.feat_num = A
		self.out_size = B
		self.row_size = C
		self.N = batch_size
		self.T = Parameter(torch.Tensor(A,B,C).to(device))
		self.reset_parameters()

	def forward(self, x):
		# Output matrices after matrix multiplication
		M = x.mm(self.T.view(self.feat_num,self.out_size*self.row_size)).view(-1,self.out_size,self.row_size)
		out = Variable(torch.zeros(self.N,self.out_size).to(device))
		for k in range(self.N): # Not happy about this 'for' loop, but this is the best we could do using PyTorch IMO
			c = torch.exp(-torch.sum(torch.abs(M[k,:]-M),2)) # exp(-L1 Norm of Rows difference)
			if k != 0 and k != self.N -1: 
				out[k,:] = torch.sum(c[0:k,:],0) + torch.sum(c[k:-1,:],0)
			else:
				if k == 0:
					out[k,:] = torch.sum(c[1:,:],0)
				else:
					out[k,:] = torch.sum(c[0:self.N-1],0)
		return out

	def reset_parameters(self):
		stddev = 1/self.feat_num
		self.T.data.uniform_(stddev)


class Generator(nn.Module):
    def __init__(self, noise_dim, data_dim):
        super().__init__()
        self.gen= nn.Sequential(
            nn.Linear(noise_dim, 2*noise_dim),
            nn.ReLU(True),
            nn.Linear(2*noise_dim, 2*noise_dim),
            nn.ReLU(True),
            nn.Linear(2*noise_dim, data_dim)
        )
    
    def forward(self, x):
        return self.gen(x)

class Softmax_block(nn.Module):
    def __init__(self, i, o):
        super().__init__()
        self.o=o
        self.block= nn.Sequential(
            nn.Linear(i,o),
            nn.Softmax(dim=1)
        )
    def forward(self, x):
        block= self.block(x)
        max_cat= torch.argmax(block, dim=1)
        #print('max cat', max_cat)
        #print('o', self.o)
        return F.one_hot(max_cat, self.o)
        


class GeneratorCat(nn.Module):
    def __init__(self, noise_dim, continuous_dim, ohe_dim):
        super().__init__()
        self.fc1=nn.Linear(noise_dim, 2*noise_dim)
        self.r1=nn.ReLU(True)
        self.fc2=nn.Linear(2*noise_dim, 2*noise_dim)
        self.r2=nn.ReLU(True)
        self.out_continuous= nn.Linear(2*noise_dim, continuous_dim)
        self.out_ohe=[]
        for s in ohe_dim:
            self.out_ohe.append(Softmax_block(2*noise_dim, s))
        #self.out_ohe= torch.cat(*self.out_ohe,1)
    
    def forward(self, x):
        f1= self.r1(self.fc1(x))
        f2= self.r2(self.fc2(f1))
        out_continuous= self.out_continuous(f2)
        out_ohe= torch.cat([sb.forward(f2) for sb in self.out_ohe],1)
        return torch.cat([out_continuous, out_ohe],1)

class Discriminator(nn.Module):
    def __init__(self, data_dim):
        super().__init__()
        self.disc= nn.Sequential(
            nn.Linear(data_dim, int(data_dim)),
            nn.LeakyReLU(0.2 ,True),
            nn.Linear(int(data_dim), int(data_dim/2)),
            nn.LeakyReLU(0.2, True),
            nn.Linear(int(data_dim/2), 1),
            nn.Sigmoid()
        )
    
    def forward(self, x):
        return self.disc(x)

class IDiscriminator(nn.Module):
    def __init__(self, data_dim):
        super().__init__()
        self.fc1= nn.Linear(data_dim, int(data_dim))
        self.r1= nn.LeakyReLU(0.2 ,True)
        self.fc2= nn.Linear(int(data_dim), int(data_dim/2))
        self.r2= nn.LeakyReLU(0.2, True)
        self.mbd = MiniBatchDiscrimination(int(data_dim/2), 1, 6, 64)
        self.out= nn.Linear(int(data_dim/2)+1, 1)
        self.sig= nn.Sigmoid()
    
    def forward(self, x):
        f1= self.r1(self.fc1(x))
        f2= self.r2(self.fc2(f1))
        return self.sig(self.out(torch.cat((f2, self.mbd(f2)),1)))

class CondinationalGenerator(nn.Module):
    def __init__(self, noise_dim, data_dim):
        super().__init__()
        self.fc1= nn.Linear(noise_dim+1, 2*noise_dim)
        self.r1= nn.ReLU(True)
        self.fc2=nn.Linear(2*noise_dim, 2*noise_dim)
        self.r2=nn.ReLU(True)
        self.out= nn.Linear(2*noise_dim, data_dim)
    
    def forward(self, x, cond):
        full= torch.cat((x,cond),1)
        f1= self.r1(self.fc1(full))
        f2= self.r2(self.fc2(f1))
        return self.out(f2)

class CondinationalGeneratorDropout(nn.Module):
    def __init__(self, noise_dim, data_dim, dropout=.5):
        super().__init__()
        self.fc1= nn.Linear(noise_dim+1, 2*noise_dim)
        self.r1= nn.ReLU(True)
        self.fc2=nn.Linear(2*noise_dim, 2*noise_dim)
        self.r2=nn.ReLU(True)
        self.out= nn.Linear(2*noise_dim, data_dim)
        self.dropout= nn.Dropout(dropout)
    
    def forward(self, x, cond):
        full= self.dropout(torch.cat((x,cond),1))
        f1= self.dropout(self.r1(self.fc1(full)))
        f2= self.dropout(self.r2(self.fc2(f1)))
        return self.out(f2)

class ConditionalGeratorCat(nn.Module):
    def __init__(self, noise_dim, continuous_dim, ohe_dim):
        super().__init__()
        self.fc1=nn.Linear(noise_dim+1, 2*noise_dim)
        self.r1=nn.ReLU(True)
        self.fc2=nn.Linear(2*noise_dim, 2*noise_dim)
        self.r2=nn.ReLU(True)
        self.out_continuous= nn.Linear(2*noise_dim, continuous_dim)
        self.out_ohe=[]
        for s in ohe_dim:
            self.out_ohe.append(Softmax_block(2*noise_dim, s).to(device))
        #self.out_ohe= torch.cat(*self.out_ohe,1)
    
    def forward(self, x, cond):
        x= torch.cat((x, cond),1)
        f1= self.r1(self.fc1(x))
        f2= self.r2(self.fc2(f1))
        out_continuous= self.out_continuous(f2)
        out_ohe= torch.cat([sb.forward(f2) for sb in self.out_ohe],1)
        return torch.cat([out_continuous, out_ohe],1)

class ConditionalDiscriminator(nn.Module):
    def __init__(self, data_dim):
        super().__init__()
        self.fc1= nn.Linear(data_dim+1, int(data_dim/2))
        self.r1= nn.LeakyReLU(0.2 ,True)
        self.fc2= nn.Linear(int(data_dim/2), int(data_dim/2))
        self.r2= nn.LeakyReLU(0.2 ,True)
        self.out= nn.Linear(int(data_dim/2), 1)
        self.sig=nn.Sigmoid()
    
    def forward(self, x, cond):
        full= torch.cat((x,cond),1)
        f1= self.r1(self.fc1(full))
        f2= self.r2(self.fc2(f1))
        return self.sig(self.out(f2))

class IConditionalDiscriminator(nn.Module):
    def __init__(self, data_dim):
        super().__init__()
        self.fc1= nn.Linear(data_dim+1, int(data_dim))
        self.r1= nn.LeakyReLU(0.2 ,True)
        self.fc2= nn.Linear(int(data_dim), int(data_dim))
        self.r2= nn.LeakyReLU(0.2 ,True)
        self.fc3= nn.Linear(int(data_dim), int(data_dim/2))
        self.r3= nn.LeakyReLU(0.2 ,True)
        self.mbd = MiniBatchDiscrimination(data_dim+1, 1, data_dim, 256)
        self.out= nn.Linear(int(data_dim/2)+1, 1)
        self.sig=nn.Sigmoid()
    
    def forward(self, x, cond):
        full= torch.cat((x,cond),1)
        f1= self.r1(self.fc1(full))
        f2= self.r2(self.fc2(f1))
        f3= self.r3(self.fc3(f2))
        return self.sig(self.out(torch.cat((f3, self.mbd(full)), 1)))

class WDiscriminator(nn.Module):
    def __init__(self, data_dim):
        super().__init__()
        self.disc= nn.Sequential(
            nn.Linear(data_dim, int(data_dim/2)),
            nn.LeakyReLU(0.2 ,True),
            nn.Linear(int(data_dim/2), int(data_dim/2)),
            nn.LeakyReLU(0.2, True),
            nn.Linear(int(data_dim/2), 1),
        )
    
    def forward(self, x):
        return self.disc(x)

class IWDiscriminator(nn.Module):
    def __init__(self, data_dim):
        super().__init__()
        self.fc1= nn.Linear(data_dim, int(data_dim))
        self.r1= nn.LeakyReLU(0.2 ,True)
        self.fc2= nn.Linear(int(data_dim), int(data_dim/2))
        self.r2= nn.LeakyReLU(0.2, True)
        self.mbd = MiniBatchDiscrimination(int(data_dim/2), 1, 6, 64)
        self.out= nn.Linear(int(data_dim/2)+1, 1)
        self.sig= nn.Sigmoid()
    
    def forward(self, x):
        f1= self.r1(self.fc1(x))
        f2= self.r2(self.fc2(f1))
        return self.out(torch.cat((f2, self.mbd(f2)),1))

class WConditionalDiscriminator(nn.Module):
    def __init__(self, data_dim):
        super().__init__()
        self.fc1= nn.Linear(data_dim+1, int(data_dim))
        self.r1= nn.LeakyReLU(0.2 ,True)
        #self.r1= nn.Tanh()
        self.fc2= nn.Linear(int(data_dim), int(data_dim))
        self.r2= nn.LeakyReLU(0.2 ,True)
        #self.r2= nn.Tanh()
        self.fc3= nn.Linear(int(data_dim), int(data_dim/2))
        #self.r3= nn.Tanh()
        self.r3= nn.LeakyReLU(0.2 ,True)
        self.out= nn.Linear(int(data_dim/2), 1)
    
    def forward(self, x, cond):
        full= torch.cat((x,cond),1)
        f1= self.r1(self.fc1(full))
        f2= self.r2(self.fc2(f1))
        f3= self.r3(self.fc3(f2))
        return self.out(f3)

class WConditionalDiscriminatorDropout(nn.Module):
    def __init__(self, data_dim, dropout=.5):
        super().__init__()
        self.fc1= nn.Linear(data_dim+1, int(data_dim))
        self.r1= nn.LeakyReLU(0.2 ,True)
        #self.r1= nn.Tanh()
        self.fc2= nn.Linear(int(data_dim), int(data_dim))
        self.r2= nn.LeakyReLU(0.2 ,True)
        #self.r2= nn.Tanh()
        self.fc3= nn.Linear(int(data_dim), int(data_dim/2))
        #self.r3= nn.Tanh()
        self.r3= nn.LeakyReLU(0.2 ,True)
        self.out= nn.Linear(int(data_dim/2), 1)
        self.dropout= nn.Dropout(dropout)
    
    def forward(self, x, cond):
        full= self.dropout(torch.cat((x,cond),1))
        f1= self.dropout(self.r1(self.fc1(full)))
        f2= self.dropout(self.r2(self.fc2(f1)))
        f3= self.dropout(self.r3(self.fc3(f2)))
        return self.out(f3)

class IWConditionalDiscriminator(nn.Module):
    def __init__(self, data_dim):
        super().__init__()
        self.fc1= nn.Linear(data_dim+1, int(data_dim))
        self.r1= nn.LeakyReLU(0.2 ,True)
        #self.r1= nn.Tanh()
        self.fc2= nn.Linear(int(data_dim), int(data_dim))
        self.r2= nn.LeakyReLU(0.2 ,True)
        #self.r2= nn.Tanh()
        self.fc3= nn.Linear(int(data_dim), int(data_dim/2))
        #self.r3= nn.Tanh()
        self.r3= nn.LeakyReLU(0.2 ,True)
        self.mbd = MiniBatchDiscrimination(int(data_dim/2), 5, 5, 64)
        self.out= nn.Linear(int(data_dim/2)+5, 1)
    
    def forward(self, x, cond):
        full= torch.cat((x,cond),1)
        f1= self.r1(self.fc1(full))
        f2= self.r2(self.fc2(f1))
        f3= self.r3(self.fc3(f2))
        return self.out(torch.cat((f3, self.mbd(f3)), 1))

class ACDiscriminator(nn.Module):
    def __init__(self, data_dim):
        super().__init__()
        self.fc1= nn.Linear(data_dim, int(data_dim))
        self.r1= nn.LeakyReLU(0.2 ,True)
        #self.r1= nn.Tanh()
        self.fc2= nn.Linear(int(data_dim), int(data_dim))
        self.r2= nn.LeakyReLU(0.2 ,True)
        #self.r2= nn.Tanh()
        self.fc3= nn.Linear(int(data_dim), int(data_dim/2))
        #self.r3= nn.Tanh()
        self.r3= nn.LeakyReLU(0.2 ,True)
        self.out= nn.Linear(int(data_dim/2), 1)
        self.aux_out= nn.Linear(int(data_dim/2),1)
        self.sig1= nn.Sigmoid()
        self.sig2= nn.Sigmoid()
    
    def forward(self, x):
        f1= self.r1(self.fc1(x))
        f2= self.r2(self.fc2(f1))
        f3= self.r3(self.fc3(f2))
        return self.sig1(self.out(f3)), self.sig2(self.aux_out(f3))

class IACDiscriminator(nn.Module):
    def __init__(self, data_dim):
        super().__init__()
        self.fc1= nn.Linear(data_dim, int(data_dim))
        self.r1= nn.LeakyReLU(0.2 ,True)
        #self.r1= nn.Tanh()
        self.fc2= nn.Linear(int(data_dim), int(data_dim))
        self.r2= nn.LeakyReLU(0.2 ,True)
        #self.r2= nn.Tanh()
        self.fc3= nn.Linear(int(data_dim), int(data_dim/2))
        #self.r3= nn.Tanh()
        self.r3= nn.LeakyReLU(0.2 ,True)
        self.mbd = MiniBatchDiscrimination(data_dim, 1, data_dim, 64)
        self.out= nn.Linear(int(data_dim/2)+1, 1)
        self.aux_out= nn.Linear(int(data_dim/2),1)
        self.sig1= nn.Sigmoid()
        self.sig2= nn.Sigmoid()
    
    def forward(self, x):
        f1= self.r1(self.fc1(x))
        f2= self.r2(self.fc2(f1))
        f3= self.r3(self.fc3(f2))
        return self.sig1(self.out(torch.cat((f3, self.mbd(x)), 1))), self.sig2(self.aux_out(f3))


class ACWDiscriminator(nn.Module):
    def __init__(self, data_dim):
        super().__init__()
        self.fc1= nn.Linear(data_dim, int(data_dim))
        self.r1= nn.LeakyReLU(0.2 ,True)
        #self.r1= nn.Tanh()
        self.fc2= nn.Linear(int(data_dim), int(data_dim))
        self.r2= nn.LeakyReLU(0.2 ,True)
        #self.r2= nn.Tanh()
        self.fc3= nn.Linear(int(data_dim), int(data_dim/2))
        #self.r3= nn.Tanh()
        self.r3= nn.LeakyReLU(0.2 ,True)
        self.out= nn.Linear(int(data_dim/2), 1)
        self.aux_out= nn.Linear(int(data_dim/2),1)
        self.sig2= nn.Sigmoid()
    
    def forward(self, x):
        f1= self.r1(self.fc1(x))
        f2= self.r2(self.fc2(f1))
        f3= self.r3(self.fc3(f2))
        return self.out(f3), self.sig2(self.aux_out(f3))

class IACWDiscriminator(nn.Module):
    def __init__(self, data_dim):
        super().__init__()
        self.fc1= nn.Linear(data_dim, int(data_dim))
        self.r1= nn.LeakyReLU(0.2 ,True)
        #self.r1= nn.Tanh()
        self.fc2= nn.Linear(int(data_dim), int(data_dim))
        self.r2= nn.LeakyReLU(0.2 ,True)
        #self.r2= nn.Tanh()
        self.fc3= nn.Linear(int(data_dim), int(data_dim/2))
        #self.r3= nn.Tanh()
        self.r3= nn.LeakyReLU(0.2 ,True)
        self.mbd = MiniBatchDiscrimination(data_dim, 1, data_dim, 256)
        self.out= nn.Linear(int(data_dim/2)+1, 1)
        self.aux_out= nn.Linear(int(data_dim/2),1)
        self.sig2= nn.Sigmoid()
    
    def forward(self, x):
        f1= self.r1(self.fc1(x))
        f2= self.r2(self.fc2(f1))
        f3= self.r3(self.fc3(f2))
        return self.out(torch.cat((f3, self.mbd(x)), 1)), self.sig2(self.aux_out(f3))


# TBD add a save weight method
class Gan():
    """wrapper over pytorch gan for simple use"""
    def __init__(self, noise_dim, data_dim, lr, disc_mul=1):
        self.noise_dim= noise_dim
        self.data_dim= data_dim
        self.lr= lr
        self.gen= Generator(self.noise_dim, self.data_dim)
        self.disc= Discriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()
        self.disc_mul= disc_mul

    
    def init_gan(self):
        self.gen.apply(weights_init)
        self.disc.apply(weights_init)
        self.criterion = nn.BCELoss()
        fixed_noise = torch.randn(self.noise_dim, device=device)
        self.optimizerD = optim.Adam(self.gen.parameters(), lr=self.lr)
        self.optimizerG = optim.Adam(self.disc.parameters(), lr=self.lr)
    
    def fit(self, x_train,batch_size, epochs):
        data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(x_train)),
                                            batch_size=batch_size, shuffle=True, drop_last=True)
        self.G_losses = []
        self.D_losses = []
        iters = 0
        # For each epoch
        for epoch in range(epochs):
            # For each batch in the dataloader
            for i, data in enumerate(data_loader, 0):

                ############################
                # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
                ###########################
                ## Train with all-real batch
                for _ in range(self.disc_mul):
                    self.disc.zero_grad()
                    b_size= data[0].size(0)
                    label= torch.ones((b_size,), dtype=torch.float, device=device)
                    output = self.disc(data[0].to(device)).view(-1)
                    errD_real = self.criterion(output, label)
                    # Calculate gradients for D in backward pass
                    errD_real.backward()
                    D_x = output.mean().item()

                    ## Train with all-fake batch
                    # Generate batch of latent vectors
                    noise = torch.randn(b_size, self.noise_dim, device=device)
                    # Generate fake image batch with G
                    fake = self.gen(noise)
                    label= torch.zeros((b_size,), dtype=torch.float, device=device)
                    # Classify all fake batch with D
                    output = self.disc(fake.detach()).view(-1)
                    # Calculate D's loss on the all-fake batch
                    errD_fake = self.criterion(output, label)
                    # Calculate the gradients for this batch, accumulated (summed) with previous gradients
                    errD_fake.backward()
                    D_G_z1 = output.mean().item()
                    # Compute error of D as sum over the fake and the real batches
                    errD = errD_real + errD_fake
                    # Update D
                    self.optimizerD.step()

                ############################
                # (2) Update G network: maximize log(D(G(z)))
                ###########################
                self.gen.zero_grad()
                label= torch.ones((b_size,), dtype=torch.float, device=device)
                # Since we just updated D, perform another forward pass of all-fake batch through D
                fake = self.gen(noise) #bizre on devrait pouvoir reutiliser l ancien fake mais ca marche pas
                output = self.disc(fake).view(-1)
                # Calculate G's loss based on this output
                errG = self.criterion(output, label)
                # Calculate gradients for G
                errG.backward()
                D_G_z2 = output.mean().item()
                # Update G
                self.optimizerG.step()

                # Output training stats
                if i ==0:
                    print('[%d/%d][%d/%d]\tLoss_D: %.4f\tLoss_G: %.4f\tD(x): %.4f\tD(G(z)): %.4f / %.4f'
                        % (epoch, epochs, i, len(data_loader),
                            errD.item(), errG.item(), D_x, D_G_z1, D_G_z2))

                # Save Losses for plotting later
                self.G_losses.append(errG.item())
                self.D_losses.append(errD.item())

                iters+=1
    
    def generate_fake(self, synth_size):
        noise= torch.randn(synth_size, self.noise_dim, device=device)
        return self.gen(noise)

class GanCat(Gan):
    def __init__(self, noise_dim, continuous_dim, ohe_dim, lr, disc_mul=1):
        self.noise_dim= noise_dim
        self.continuous_dim= continuous_dim
        self.ohe_dim= ohe_dim
        self.data_dim= sum(self.ohe_dim)+ continuous_dim
        self.lr= lr
        self.gen= GeneratorCat(self.noise_dim, self.continuous_dim, self.ohe_dim)
        self.disc= Discriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()
        self.disc_mul= disc_mul

class IGan(Gan):
    def __init__(self, noise_dim, data_dim, lr, disc_mul=4):
        super().__init__(noise_dim, data_dim, lr)
        self.gen= Generator(self.noise_dim, self.data_dim)
        self.disc= IDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()

class WGan(Gan):
    def __init__(self, noise_dim, data_dim, lr, disc_mul=3):
        super().__init__(noise_dim, data_dim, lr)
        self.disc_mul= disc_mul
        self.gen= Generator(self.noise_dim, self.data_dim)
        self.disc= WDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
    
    def fit(self, x_train,batch_size, epochs):
        data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(x_train)),
                                            batch_size=batch_size, shuffle=True)
        self.G_losses = []
        self.D_losses = []
        iters = 0
        # For each epoch
        for epoch in range(epochs):
            # For each batch in the dataloader
            for i, data in enumerate(data_loader, 0):

                ############################
                # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
                ###########################
                ## Train with all-real batch
                for _ in range(self.disc_mul):
                    self.disc.zero_grad()
                    for p in self.disc.parameters():
                        p.data.clamp_(-0.01, 0.01)
                    b_size= data[0].size(0)
                    #label= torch.ones((b_size,), dtype=torch.float, device=device)
                    output = self.disc(data[0].to(device)).view(-1)
                    errD_real = output.mean()
                    # Calculate gradients for D in backward pass
                    errD_real.backward()
                    #D_x = output.mean().item()

                    ## Train with all-fake batch
                    # Generate batch of latent vectors
                    noise = torch.randn(b_size, self.noise_dim, device=device)
                    # Generate fake image batch with G
                    fake = self.gen(noise)
                    label= torch.zeros((b_size,), dtype=torch.float, device=device)
                    # Classify all fake batch with D
                    output = self.disc(fake.detach()).view(-1)
                    # Calculate D's loss on the all-fake batch
                    errD_fake = -output.mean()
                    # Calculate the gradients for this batch, accumulated (summed) with previous gradients
                    errD_fake.backward()
                    #D_G_z1 = output.mean().item()
                    # Compute error of D as sum over the fake and the real batches
                    errW = errD_real + errD_fake
                    # Update D
                    self.optimizerD.step()

                ############################
                # (2) Update G network: maximize log(D(G(z)))
                ###########################
                self.gen.zero_grad()
                label= torch.ones((b_size,), dtype=torch.float, device=device)
                # Since we just updated D, perform another forward pass of all-fake batch through D
                fake = self.gen(noise) #bizre on devrait pouvoir reutiliser l ancien fake mais ca marche pas
                output = self.disc(fake).view(-1)
                # Calculate G's loss based on this output
                errG = output.mean()
                # Calculate gradients for G
                errG.backward()
                # Update G
                self.optimizerG.step()

                # Output training stats
                if i % 200 == 0:
                    print('[%d/%d][%d/%d]\tLoss_D: %.4f\tLoss_G: %.4f'
                        % (epoch, epochs, i, len(data_loader),
                            errW.item(), errG.item()))

                # Save Losses for plotting later
                self.G_losses.append(errG.item())
                self.D_losses.append(errW.item())

                iters+=1

class IWGan(WGan):
    def __init__(self, noise_dim, data_dim, lr, disc_mul=4):
        super().__init__(noise_dim, data_dim, lr)
        self.gen= CondinationalGenerator(self.noise_dim, self.data_dim)
        self.disc= IConditionalDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()

class CGan(Gan):
    def __init__(self, noise_dim, data_dim, lr, disc_mul=3):
        super().__init__(noise_dim, data_dim, lr)
        self.gen= CondinationalGenerator(self.noise_dim, self.data_dim)
        self.disc= ConditionalDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()
        self.disc_mul= disc_mul
    
    def fit(self, x_train, cond,batch_size, epochs):
        data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(x_train), torch.FloatTensor(cond).reshape(-1,1)),
                                            batch_size=batch_size, shuffle=True, drop_last=True)
        self.G_losses = []
        self.D_losses = []
        iters = 0
        # For each epoch
        for epoch in range(epochs):
            # For each batch in the dataloader
            for i, data in enumerate(data_loader, 0):

                ############################
                # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
                ###########################
                ## Train with all-real batch
                for _ in range(self.disc_mul):
                    print(_)
                    self.optimizerD.zero_grad()
                    b_size= data[0].size(0)
                    label= torch.ones((b_size,), dtype=torch.float, device=device)
                    output = self.disc.forward(data[0].to(device), data[1].to(device)).view(-1)
                    errD_real = self.criterion(output, label)
                    # Calculate gradients for D in backward pass
                    errD_real.backward()
                    D_x = output.mean().item()

                    ## Train with all-fake batch
                    # Generate batch of latent vectors
                    noise = torch.randn(b_size, self.noise_dim, device=device)
                    # Generate fake image batch with G
                    fake = self.gen.forward(noise, data[1].to(device))
                    label= torch.zeros((b_size,), dtype=torch.float, device=device)
                    # Classify all fake batch with D
                    output = self.disc(fake.detach(), data[1].to(device)).view(-1)
                    # Calculate D's loss on the all-fake batch
                    errD_fake = self.criterion(output, label)
                    # Calculate the gradients for this batch, accumulated (summed) with previous gradients
                    errD_fake.backward()
                    D_G_z1 = output.mean().item()
                    # Compute error of D as sum over the fake and the real batches
                    errD = errD_real + errD_fake
                    # Update D
                    self.optimizerD.step()

                ############################
                # (2) Update G network: maximize log(D(G(z)))
                ###########################
                self.optimizerG.zero_grad()
                label= torch.ones((b_size,), dtype=torch.float, device=device)
                # Since we just updated D, perform another forward pass of all-fake batch through D
                fake = self.gen(noise, data[1].to(device)) #bizre on devrait pouvoir reutiliser l ancien fake mais ca marche pas
                output = self.disc(fake, data[1].to(device)).view(-1)
                # Calculate G's loss based on this output
                errG = self.criterion(output, label)
                # Calculate gradients for G
                errG.backward()
                D_G_z2 = output.mean().item()
                # Update G
                self.optimizerG.step()

                # Output training stats
                if i  == 0:
                    print('[%d/%d][%d/%d]\tLoss_D: %.4f\tLoss_G: %.4f\tD(x): %.4f\tD(G(z)): %.4f / %.4f'
                        % (epoch, epochs, i, len(data_loader),
                            errD.item(), errG.item(), D_x, D_G_z1, D_G_z2))

                # Save Losses for plotting later
                self.G_losses.append(errG.item())
                self.D_losses.append(errD.item())

                iters+=1
    
    def generate_fake(self, cond):
        noise= torch.randn(cond.shape[0], self.noise_dim, device=device)
        return self.gen(noise, torch.FloatTensor(cond).reshape(-1,1).to(device))

class CGanCat(CGan):
    def __init__(self, noise_dim, continuous_dim, ohe_dim, lr, disc_mul=1):
        self.noise_dim= noise_dim
        self.continuous_dim= continuous_dim
        self.ohe_dim= ohe_dim
        self.data_dim= sum(self.ohe_dim)+ continuous_dim
        self.lr= lr
        self.gen= ConditionalGeratorCat(self.noise_dim, self.continuous_dim, self.ohe_dim)
        self.disc= ConditionalDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()
        self.disc_mul= disc_mul

class ICGanCat(CGanCat):
    def __init__(self, noise_dim, continuous_dim, ohe_dim, lr, disc_mul=1):
        self.noise_dim= noise_dim
        self.continuous_dim= continuous_dim
        self.ohe_dim= ohe_dim
        self.data_dim= sum(self.ohe_dim)+ continuous_dim
        self.lr= lr
        self.gen= ConditionalGeratorCat(self.noise_dim, self.continuous_dim, self.ohe_dim)
        self.disc= IConditionalDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()
        self.disc_mul= disc_mul

class ICGan(CGan):
    def __init__(self, noise_dim, data_dim, lr, disc_mul=4):
        super().__init__(noise_dim, data_dim, lr)
        self.gen= CondinationalGenerator(self.noise_dim, self.data_dim)
        self.disc= IConditionalDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()


class WCGan(CGan):
    def __init__(self, noise_dim, data_dim, lr, disc_mul=3):
        super().__init__(noise_dim, data_dim, lr)
        self.gen= CondinationalGenerator(self.noise_dim, self.data_dim)
        self.disc= WConditionalDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()
        self.init_gan()
        self.disc_mul= disc_mul
    
    def fit(self, x_train, cond,batch_size, epochs):
        data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(x_train), torch.FloatTensor(cond).reshape(-1,1)),
                                            batch_size=batch_size, shuffle=True, drop_last=True)
        self.G_losses = []
        self.D_losses = []
        iters = 0
        # For each epoch
        for epoch in range(epochs):
            # For each batch in the dataloader
            for i, data in enumerate(data_loader, 0):

                ############################
                # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
                ###########################
                ## Train with all-real batch
                for _ in range(self.disc_mul):
                    for p in self.disc.parameters():
                        p.requires_grad = True
                    self.disc.zero_grad()
                    for p in self.disc.parameters():
                        p.data.clamp_(-0.01, 0.01)
                    b_size= data[0].size(0)
                    label= torch.ones((b_size,), dtype=torch.float, device=device)
                    output = self.disc.forward(data[0].to(device), data[1].to(device)).view(-1)
                    errD_real = output.mean()
                    errD_real_log= errD_real.item()
                    # Calculate gradients for D in backward pass
                    errD_real.backward()
                    D_x = output.mean().item()

                    ## Train with all-fake batch
                    # Generate batch of latent vectors
                    noise = torch.randn(b_size, self.noise_dim, device=device)
                    # Generate fake image batch with G
                    fake = self.gen.forward(noise, data[1].to(device))
                    label= torch.zeros((b_size,), dtype=torch.float, device=device)
                    # Classify all fake batch with D
                    output = self.disc(fake.detach(), data[1].to(device)).view(-1)
                    # Calculate D's loss on the all-fake batch
                    errD_fake = -output.mean()
                    errD_fake_log= errD_fake.item()
                    # Calculate the gradients for this batch, accumulated (summed) with previous gradients
                    errD_fake.backward()
                    D_G_z1 = output.mean().item()
                    # Compute error of D as sum over the fake and the real batches
                    errW = errD_real + errD_fake
                    #errW.backward()
                    # Update D
                    self.optimizerD.step()

                ############################
                # (2) Update G network: maximize log(D(G(z)))
                ###########################
                for p in self.disc.parameters():
                    p.requires_grad = False  # to avoid computation
                self.gen.zero_grad()
                label= torch.ones((b_size,), dtype=torch.float, device=device)
                # Since we just updated D, perform another forward pass of all-fake batch through D
                fake = self.gen(noise, data[1].to(device)) #bizre on devrait pouvoir reutiliser l ancien fake mais ca marche pas
                output = self.disc(fake, data[1].to(device)).view(-1)
                # Calculate G's loss based on this output
                #errG = self.criterion(output, label)
                #MODIFIED loss
                errG= output.mean()
                # Calculate gradients for G
                errG.backward()
                D_G_z2 = output.mean().item()
                # Update G
                self.optimizerG.step()

                # Output training stats
                if i == 0:
                    print('[%d/%d][%d/%d]\tLoss_D: %.4f  lossD_real:  %.4f   lossD_fake:  %.4f\tLoss_G: %.4f\tD(x): %.4f\tD(G(z)): %.4f / %.4f'
                        % (epoch, epochs, i, len(data_loader),
                            0, errD_real_log,errD_fake_log,errG.item(), D_x, D_G_z1, D_G_z2))

                # Save Losses for plotting later
                self.G_losses.append(errG.item())
                self.D_losses.append(errW.item())

                iters+=1
    
    def generate_fake(self, cond):
        noise= torch.randn(cond.shape[0], self.noise_dim, device=device)
        return self.gen(noise, torch.FloatTensor(cond).reshape(-1,1).to(device))


class WCGanDropout(WCGan):
    def __init__(self, noise_dim, data_dim, lr, dropout=.5,disc_mul=3):
        super().__init__(noise_dim, data_dim, lr)
        self.gen= CondinationalGeneratorDropout(self.noise_dim, self.data_dim,dropout)
        self.disc= WConditionalDiscriminatorDropout(self.data_dim, dropout)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()
        self.init_gan()
        self.disc_mul= disc_mul

class WCGanDP(WCGan):
    def __init__(self, noise_dim, data_dim, lr, epsilon, delta, grad_norm ,disc_mul=3):
        super().__init__(noise_dim, data_dim, lr, disc_mul)
        self.epsilon= epsilon
        self.delta= delta
        self.grad_norm= grad_norm
    
    def fit(self, x_train, cond,batch_size, epochs):
        data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(x_train), torch.FloatTensor(cond).reshape(-1,1)),
                                            batch_size=batch_size, shuffle=True, drop_last=True)
        self.G_losses = []
        self.D_losses = []
        iters = 0
        self.privacy_engine = PrivacyEngine()
        self.model, self.optimizerD, data_loader = self.privacy_engine.make_private_with_epsilon(
            module=self.disc,
            optimizer=self.optimizerD,
            data_loader=data_loader,
            target_delta=self.delta,
            target_epsilon=self.epsilon, 
            epochs=epochs,
            max_grad_norm=self.grad_norm,
        )
        # For each epoch
        for epoch in range(epochs):
            # For each batch in the dataloader
            for i, data in enumerate(data_loader, 0):

                ############################
                # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
                ###########################
                ## Train with all-real batch
                for _ in range(self.disc_mul):
                    for p in self.disc.parameters():
                        p.requires_grad = True
                    self.disc.zero_grad()
                    for p in self.disc.parameters():
                        p.data.clamp_(-0.01, 0.01)
                    b_size= data[0].size(0)
                    label= torch.ones((b_size,), dtype=torch.float, device=device)
                    output = self.disc.forward(data[0].to(device), data[1].to(device)).view(-1)
                    errD_real = output.mean()
                    errD_real_log= errD_real.item()
                    # Calculate gradients for D in backward pass
                    #errD_real.backward()
                    D_x = output.mean().item()

                    ## Train with all-fake batch
                    # Generate batch of latent vectors
                    noise = torch.randn(b_size, self.noise_dim, device=device)
                    # Generate fake image batch with G
                    fake = self.gen.forward(noise, data[1].to(device))
                    label= torch.zeros((b_size,), dtype=torch.float, device=device)
                    # Classify all fake batch with D
                    output = self.disc(fake.detach(), data[1].to(device)).view(-1)
                    # Calculate D's loss on the all-fake batch
                    errD_fake = -output.mean()
                    errD_fake_log= errD_fake.item()
                    # Calculate the gradients for this batch, accumulated (summed) with previous gradients
                    #errD_fake.backward()
                    D_G_z1 = output.mean().item()
                    # Compute error of D as sum over the fake and the real batches
                    errW = errD_real + errD_fake
                    errW.backward()
                    # Update D
                    self.optimizerD.step()

                ############################
                # (2) Update G network: maximize log(D(G(z)))
                ###########################
                for p in self.disc.parameters():
                    p.requires_grad = False  # to avoid computation
                self.gen.zero_grad()
                label= torch.ones((b_size,), dtype=torch.float, device=device)
                # Since we just updated D, perform another forward pass of all-fake batch through D
                fake = self.gen(noise, data[1].to(device)) #bizre on devrait pouvoir reutiliser l ancien fake mais ca marche pas
                output = self.disc(fake, data[1].to(device)).view(-1)
                # Calculate G's loss based on this output
                #errG = self.criterion(output, label)
                #MODIFIED loss
                errG= output.mean()
                # Calculate gradients for G
                errG.backward()
                D_G_z2 = output.mean().item()
                # Update G
                self.optimizerG.step()

                # Output training stats
                if i == 0:
                    print('[%d/%d][%d/%d]\tLoss_D: %.4f  lossD_real:  %.4f   lossD_fake:  %.4f\tLoss_G: %.4f\tD(x): %.4f\tD(G(z)): %.4f / %.4f'
                        % (epoch, epochs, i, len(data_loader),
                            errW.item(), errD_real_log,errD_fake_log,errG.item(), D_x, D_G_z1, D_G_z2))

                # Save Losses for plotting later
                self.G_losses.append(errG.item())
                self.D_losses.append(errW.item())

                iters+=1
    

class WCGanCat(WCGan):
    def __init__(self, noise_dim, continuous_dim, ohe_dim, lr, disc_mul=3):
        self.noise_dim= noise_dim
        self.continuous_dim= continuous_dim
        self.ohe_dim= ohe_dim
        self.data_dim= sum(self.ohe_dim)+ continuous_dim
        self.lr= lr
        self.gen= ConditionalGeratorCat(self.noise_dim, self.continuous_dim, self.ohe_dim)
        self.disc= WConditionalDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()
        self.disc_mul= disc_mul


class IWCGan(CGan):
    def __init__(self, noise_dim, data_dim, lr, disc_mul=3):
        super().__init__(noise_dim, data_dim, lr)
        self.gen= CondinationalGenerator(self.noise_dim, self.data_dim)
        self.disc= IWConditionalDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()

    
    def fit(self, x_train, cond,batch_size, epochs):
        data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(x_train), torch.FloatTensor(cond).reshape(-1,1)),
                                            batch_size=batch_size, shuffle=True, drop_last=True)
        self.G_losses = []
        self.D_losses = []
        iters = 0
        # For each epoch
        for epoch in range(epochs):
            # For each batch in the dataloader
            for i, data in enumerate(data_loader, 0):

                ############################
                # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
                ###########################
                ## Train with all-real batch
                for _ in range(self.disc_mul):
                    for p in self.disc.parameters():
                        p.requires_grad = True
                    self.disc.zero_grad()
                    for p in self.disc.parameters():
                        p.data.clamp_(-0.01, 0.01)
                    b_size= data[0].size(0)
                    label= torch.ones((b_size,), dtype=torch.float, device=device)
                    output = self.disc.forward(data[0].to(device), data[1].to(device)).view(-1)
                    errD_real = output.mean()
                    errD_real_log= errD_real.item()
                    # Calculate gradients for D in backward pass
                    #errD_real.backward()
                    D_x = output.mean().item()

                    ## Train with all-fake batch
                    # Generate batch of latent vectors
                    noise = torch.randn(b_size, self.noise_dim, device=device)
                    # Generate fake image batch with G
                    fake = self.gen.forward(noise, data[1].to(device))
                    label= torch.zeros((b_size,), dtype=torch.float, device=device)
                    # Classify all fake batch with D
                    output = self.disc(fake.detach(), data[1].to(device)).view(-1)
                    # Calculate D's loss on the all-fake batch
                    errD_fake = -output.mean()
                    errD_fake_log= errD_fake.item()
                    # Calculate the gradients for this batch, accumulated (summed) with previous gradients
                    #errD_fake.backward()
                    D_G_z1 = output.mean().item()
                    # Compute error of D as sum over the fake and the real batches
                    errW = errD_real + errD_fake
                    errW.backward()
                    # Update D
                    self.optimizerD.step()

                ############################
                # (2) Update G network: maximize log(D(G(z)))
                ###########################
                for p in self.disc.parameters():
                    p.requires_grad = False  # to avoid computation
                self.gen.zero_grad()
                label= torch.ones((b_size,), dtype=torch.float, device=device)
                # Since we just updated D, perform another forward pass of all-fake batch through D
                fake = self.gen(noise, data[1].to(device)) #bizre on devrait pouvoir reutiliser l ancien fake mais ca marche pas
                output = self.disc(fake, data[1].to(device)).view(-1)
                # Calculate G's loss based on this output
                #errG = self.criterion(output, label)
                #MODIFIED loss
                errG= output.mean()
                # Calculate gradients for G
                errG.backward()
                D_G_z2 = output.mean().item()
                # Update G
                self.optimizerG.step()

                # Output training stats
                if i % 200 == 0:
                    print('[%d/%d][%d/%d]\tLoss_D: %.4f  lossD_real:  %.4f   lossD_fake:  %.4f\tLoss_G: %.4f\tD(x): %.4f\tD(G(z)): %.4f / %.4f'
                        % (epoch, epochs, i, len(data_loader),
                            errW.item(), errD_real_log,errD_fake_log,errG.item(), D_x, D_G_z1, D_G_z2))

                # Save Losses for plotting later
                self.G_losses.append(errG.item())
                self.D_losses.append(errW.item())

                iters+=1
    
    def generate_fake(self, cond):
        noise= torch.randn(cond.shape[0], self.noise_dim, device=device)
        return self.gen(noise, torch.FloatTensor(cond).reshape(-1,1).to(device))

class ACGan(CGan):
    def __init__(self, noise_dim, data_dim, lr, disc_mul=3, ac_weight=1):
        super().__init__(noise_dim, data_dim, lr)
        self.gen= CondinationalGenerator(self.noise_dim, self.data_dim)
        self.disc= ACDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()
        self.disc_mul= disc_mul
        self.ac_weight= ac_weight
    
    def fit(self, x_train, cond,batch_size, epochs):
        data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(x_train), torch.FloatTensor(cond).reshape(-1,1)),
                                            batch_size=batch_size, shuffle=True, drop_last=True)
        self.G_losses = []
        self.D_losses = []
        iters = 0
        # For each epoch
        for epoch in range(epochs):
            # For each batch in the dataloader
            for i, data in enumerate(data_loader, 0):

                ############################
                # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
                ###########################
                ## Train with all-real batch
                for _ in range(self.disc_mul):
                    self.disc.zero_grad()
                    b_size= data[0].size(0)
                    label= torch.ones((b_size,), dtype=torch.float, device=device)
                    output, pred = self.disc.forward(data[0].to(device))
                    output= output.view(-1)
                    errD_real = self.criterion(output, label)
                    errD_ac_real= self.criterion(pred, data[1].to(device))
                    # Calculate gradients for D in backward pass
                    errD_real_total= errD_real+ errD_ac_real 
                    errD_real_total.backward()
                    D_x = output.mean().item()

                    ## Train with all-fake batch
                    # Generate batch of latent vectors
                    noise = torch.randn(b_size, self.noise_dim, device=device)
                    # Generate fake image batch with G
                    fake = self.gen.forward(noise, data[1].to(device))
                    label= torch.zeros((b_size,), dtype=torch.float, device=device)
                    # Classify all fake batch with D
                    output, pred = self.disc(fake.detach())
                    output= output.view(-1)
                    # Calculate D's loss on the all-fake batch
                    errD_fake = self.criterion(output, label)
                    errD_ac_fake= self.criterion(pred, data[1].to(device))
                    # Calculate the gradients for this batch, accumulated (summed) with previous gradients
                    if epoch<=20:
                        errD_fake_total= errD_fake+ errD_ac_fake 
                    else:
                        errD_fake_total= errD_fake+ errD_ac_fake #* (4/epoch)
                    errD_fake_total.backward()
                    # Compute error of D as sum over the fake and the real batches
                    errD = errD_real_total + errD_fake_total
                    # Update D
                    self.optimizerD.step()

                ############################
                # (2) Update G network: maximize log(D(G(z)))
                ###########################
                self.gen.zero_grad()
                label= torch.ones((b_size,), dtype=torch.float, device=device)
                # Since we just updated D, perform another forward pass of all-fake batch through D
                fake = self.gen(noise, data[1].to(device)) #bizre on devrait pouvoir reutiliser l ancien fake mais ca marche pas
                output, pred = self.disc(fake)
                output= output.view(-1)
                # Calculate G's loss based on this output
                errG = self.criterion(output, label)
                errG_ac= self.criterion(pred, data[1].to(device))
                # Calculate gradients for G
                if epoch<=20:
                    errG_total= errG + errG_ac *self.ac_weight
                else:
                    errG_total= errG + errG_ac #* (4/epoch)
                errG_total.backward()
                D_G_z2 = output.mean().item()
                # Update G
                self.optimizerG.step()

                # Output training stats
                if i == 0:
                    print('[%d/%d][%d/%d]\tLoss_D_total: %.4f\t loss_D: %.4f \t loss_AC_real: %.4f  Loss_G_total: %.4f\t Loss_G: %.4f\t loss_AC_fake: %.4f'
                        % (epoch, epochs, i, len(data_loader),
                            errD.item(), (errD_real+errD_fake).item(), errD_ac_real.item(), errG_total.item(),errG.item(), errG_ac.item()))

                # Save Losses for plotting later
                self.G_losses.append(errG_total.item())
                self.D_losses.append(errD.item())

                iters+=1

class ACGanCat(ACGan):
    def __init__(self, noise_dim, continuous_dim, ohe_dim, lr, disc_mul=1, ac_weight=1):
        self.noise_dim= noise_dim
        self.continuous_dim= continuous_dim
        self.ohe_dim= ohe_dim
        self.data_dim= sum(self.ohe_dim)+ continuous_dim
        self.lr= lr
        self.gen= ConditionalGeratorCat(self.noise_dim, self.continuous_dim, self.ohe_dim)
        self.disc= ACDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()
        self.disc_mul= disc_mul
        self.ac_weight= ac_weight

class ACWGan(ACGan):
    def __init__(self, noise_dim, data_dim, lr, disc_mul=4, ac_weight=1):
        super().__init__(noise_dim, data_dim, lr)
        self.gen= CondinationalGenerator(self.noise_dim, self.data_dim)
        self.disc= ACWDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()
        self.disc_mul= disc_mul
        self.ac_weight= ac_weight
    
    def fit(self, x_train, cond,batch_size, epochs):
        data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(x_train), torch.FloatTensor(cond).reshape(-1,1)),
                                            batch_size=batch_size, shuffle=True, drop_last=True)
        self.G_losses = []
        self.D_losses = []
        iters = 0
        # For each epoch
        for epoch in range(epochs):
            # For each batch in the dataloader
            for i, data in enumerate(data_loader, 0):

                ############################
                # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
                ###########################
                ## Train with all-real batch
                for _ in range(self.disc_mul):
                    self.disc.zero_grad()
                    for p in self.disc.parameters():
                        p.data.clamp_(-0.01, 0.01)
                    b_size= data[0].size(0)
                    label= torch.ones((b_size,), dtype=torch.float, device=device)
                    output, pred = self.disc.forward(data[0].to(device))
                    output= output.view(-1)
                    errD_real = output.mean()
                    errD_ac_real= self.criterion(pred, data[1].to(device))
                    # Calculate gradients for D in backward pass
                    errD_real_total= errD_real+ errD_ac_real 
                    errD_real_total.backward()
                    D_x = output.mean().item()

                    ## Train with all-fake batch
                    # Generate batch of latent vectors
                    noise = torch.randn(b_size, self.noise_dim, device=device)
                    # Generate fake image batch with G
                    fake = self.gen.forward(noise, data[1].to(device))
                    label= torch.zeros((b_size,), dtype=torch.float, device=device)
                    # Classify all fake batch with D
                    output, pred = self.disc(fake.detach())
                    output= output.view(-1)
                    # Calculate D's loss on the all-fake batch
                    errD_fake = - output.mean()
                    errD_ac_fake= self.criterion(pred, data[1].to(device))
                    # Calculate the gradients for this batch, accumulated (summed) with previous gradients
                    if epoch<=20:
                        errD_fake_total= errD_fake+ errD_ac_fake 
                    else:
                        errD_fake_total= errD_fake+ errD_ac_fake #* (4/epoch)                    
                    errD_fake_total.backward()
                    # Compute error of D as sum over the fake and the real batches
                    errD = errD_real_total + errD_fake_total * self.ac_weight
                    # Update D
                    self.optimizerD.step()

                ############################
                # (2) Update G network: maximize log(D(G(z)))
                ###########################
                self.gen.zero_grad()
                label= torch.ones((b_size,), dtype=torch.float, device=device)
                # Since we just updated D, perform another forward pass of all-fake batch through D
                fake = self.gen(noise, data[1].to(device)) #bizre on devrait pouvoir reutiliser l ancien fake mais ca marche pas
                output, pred = self.disc(fake)
                output= output.view(-1)
                # Calculate G's loss based on this output
                errG = output.mean()
                errG_ac= self.criterion(pred, data[1].to(device))
                # Calculate gradients for G
                if epoch<=20:
                    errG_total= errG + errG_ac *self.ac_weight
                else:
                    errG_total= errG + errG_ac #*(4/epoch)
                errG_total.backward()
                D_G_z2 = output.mean().item()
                # Update G
                self.optimizerG.step()

                # Output training stats
                if i == 0:
                    print('[%d/%d][%d/%d]\tLoss_D_total: %.4f\t loss_D: %.4f \t loss_AC_real: %.4f  Loss_G_total: %.4f\t Loss_G: %.4f\t loss_AC_fake: %.4f'
                        % (epoch, epochs, i, len(data_loader),
                            errD.item(), (errD_real+errD_fake).item(), errD_ac_real.item(), errG_total.item(),errG.item(), errG_ac.item()))

                # Save Losses for plotting later
                self.G_losses.append(errG_total.item())
                self.D_losses.append(errD.item())

                iters+=1

class IACGan(ACGan):
    def __init__(self, noise_dim, data_dim, lr, disc_mul=3, ac_weight=1):
        super().__init__(noise_dim, data_dim, lr)
        self.gen= CondinationalGenerator(self.noise_dim, self.data_dim)
        self.disc= IACDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()
        self.ac_weight= ac_weight

class IACGanCat(IACGan):
    def __init__(self, noise_dim, continuous_dim, ohe_dim, lr, disc_mul=1, ac_weight=1):
        self.noise_dim= noise_dim
        self.continuous_dim= continuous_dim
        self.ohe_dim= ohe_dim
        self.data_dim= sum(self.ohe_dim)+ continuous_dim
        self.lr= lr
        self.gen= ConditionalGeratorCat(self.noise_dim, self.continuous_dim, self.ohe_dim)
        self.disc= IACDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()
        self.disc_mul= disc_mul
        self.ac_weight= ac_weight

class IACWGanCat(ACWGan):
    def __init__(self, noise_dim, continuous_dim, ohe_dim, lr, disc_mul=1):
        self.noise_dim= noise_dim
        self.continuous_dim= continuous_dim
        self.ohe_dim= ohe_dim
        self.data_dim= sum(self.ohe_dim)+ continuous_dim
        self.lr= lr
        self.gen= ConditionalGeratorCat(self.noise_dim, self.continuous_dim, self.ohe_dim)
        self.disc= IACWDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()
        self.disc_mul= disc_mul

class ACWGanCat(ACWGan):
    def __init__(self, noise_dim, continuous_dim, ohe_dim, lr, disc_mul=3):
        self.noise_dim= noise_dim
        self.continuous_dim= continuous_dim
        self.ohe_dim= ohe_dim
        self.data_dim= sum(self.ohe_dim)+ continuous_dim
        self.lr= lr
        self.gen= ConditionalGeratorCat(self.noise_dim, self.continuous_dim, self.ohe_dim)
        self.disc= ACWDiscriminator(self.data_dim)
        self.gen.to(device)
        self.disc.to(device)
        self.init_gan()
        self.disc_mul= disc_mul
