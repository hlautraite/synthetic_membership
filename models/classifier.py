import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
from sklearn.model_selection import cross_val_score
from sklearn.metrics import precision_recall_curve, plot_precision_recall_curve, f1_score, roc_auc_score
import matplotlib.pyplot as plt
from lightgbm import LGBMClassifier


#Ajouter sample weight

def reg_log_train_predict(train_x, train_y, test_x):
    lr= LogisticRegression(random_state=42)
    lr.fit(train_x, train_y)
    return lr.predict_proba(test_x)[:,1], lr

class DecisionTree():
    def __init__(self, train_x, train_y, test_x):
        self.train_x= train_x
        self.train_y= train_y
        self.test_x= test_x
        self.init_hyp_param()
    
    def init_hyp_param(self):
        self.hyp_param= {
            'max_depth': hp.choice('max_depth', range(1,7)),
            #"criterion": hp.choice("criterion", ["gini", "entropy"])
            'class_weight': hp.choice('class_weight', [None, 'balanced'])

        }
    
    def train_test(self, params):
        tree= DecisionTreeClassifier(**params, random_state=42)
        return {'loss':-cross_val_score(tree, self.train_x, self.train_y, cv= 3, scoring='f1', n_jobs=-1).mean(), 'status': STATUS_OK}
    
    def find_best_params(self):
        trials = Trials()
        print('starting hyperopt')
        self.best_hp = fmin(self.train_test, self.hyp_param, algo=tpe.suggest, max_evals=10, trials=trials, return_argmin=False)
        print('best hyper-parameters found:', self.best_hp)
    
    def train_predict(self):
        self.find_best_params()
        print('final training')
        self.best_tree= DecisionTreeClassifier(**self.best_hp, random_state=42)
        self.best_tree.fit(self.train_x, self.train_y)
        return self.best_tree.predict_proba(self.test_x)[:,1]

class RandomForest():
    def __init__(self, train_x, train_y, test_x):
        self.train_x= train_x
        self.train_y= train_y
        self.test_x= test_x
        self.init_hyp_param()
    
    def init_hyp_param(self):
        self.hyp_param= {
            'max_depth': hp.choice('max_depth', range(1,7)),
            'max_features': hp.choice('max_features', ['sqrt', 'log2', 4,5,7]),
            #'n_estimators': hp.choice('n_estimators', range(1,200)),
            'n_estimators': hp.choice('n_estimators', [10,20,30,40,50,75,100,125,150,175,200]),
            'class_weight': hp.choice('class_weight', [None, 'balanced'])
        }
    
    def train_test(self, params):
        tree= RandomForestClassifier(**params, random_state=42, n_jobs=-1)
        return {'loss':-cross_val_score(tree, self.train_x, self.train_y, cv= 3, scoring='f1', n_jobs=-1).mean(), 'status': STATUS_OK}
    
    def find_best_params(self):
        trials = Trials()
        print('starting hyperopt')
        self.best_hp = fmin(self.train_test, self.hyp_param, algo=tpe.suggest, max_evals=20, trials=trials, return_argmin=False)
        print('best hyper-parameters found:', self.best_hp)
    
    def train_predict(self):
        self.find_best_params()
        print('final training')
        self.best_rf= RandomForestClassifier(**self.best_hp, random_state=42)
        self.best_rf.fit(self.train_x, self.train_y)
        return self.best_rf.predict_proba(self.test_x)[:,1]

class GradientBoosting(): #use light GBM with balanced option instead
    def __init__(self, train_x, train_y, test_x):
        self.train_x= train_x
        self.train_y= train_y
        self.test_x= test_x
        self.init_hyp_param()
    
    def init_hyp_param(self):
        self.hyp_param= {
            'max_depth': hp.choice('max_depth', range(1,7)),
            #'max_features': hp.choice('max_features', ['sqrt', 'log2', 4,5,7]),
            'n_estimators': hp.choice('n_estimators', [10,20,30,40,50,75,100,125,150,175,200]),
            'learning_rate': hp.choice('learning_rate', [0.3,0.1, 0.05,0.01, 0.005, 0.001, 0.0005, 0.0001])
            ,'class_weight': hp.choice('class_weight', [None, 'balanced'])
        }
    
    def train_test(self, params):
        gb= LGBMClassifier(**params, random_state=42)
        #gb= GradientBoostingClassifier(**params, random_state=42)
        return {'loss':-cross_val_score(gb, self.train_x, self.train_y, cv= 3, scoring='f1', n_jobs=-1).mean(), 'status': STATUS_OK}
    
    def find_best_params(self):
        trials = Trials()
        print('starting hyperopt')
        self.best_hp = fmin(self.train_test, self.hyp_param, algo=tpe.suggest, max_evals=20, trials=trials, return_argmin=False)
        print('best hyper-parameters found:', self.best_hp)
    
    def train_predict(self):
        self.find_best_params()
        print('final training')
        self.best_gb= LGBMClassifier(**self.best_hp, random_state=42)
        self.best_gb.fit(self.train_x, self.train_y)
        return self.best_gb.predict_proba(self.test_x)[:,1]


class ModelComparaison():
    def __init__(self, train_x, train_y, test_x, test_y, train_synth_x, train_synth_y):
        self.train_x= train_x
        self.train_y= train_y
        self.train_syth_x= train_synth_x
        self.train_syth_y= train_synth_y
        self.test_x= test_x
        self.test_y= test_y
        self.perf_comparaison= {}
    
    def init_models(self):
        self.lr_pred, self.lr= reg_log_train_predict(self.train_x, self.train_y, self.test_x)
        self.synth_lr_pred, self.lr_synth= reg_log_train_predict(self.train_syth_x, self.train_syth_y, self.test_x)
        self.dt= DecisionTree(self.train_x, self.train_y, self.test_x)
        self.tree_pred= self.dt.train_predict()
        self.dt_synth= DecisionTree(self.train_syth_x, self.train_syth_y, self.test_x)
        self.synth_tree_pred= self.dt_synth.train_predict()
        self.rf= RandomForest(self.train_x, self.train_y, self.test_x)
        self.rf_pred= self.rf.train_predict()
        self.rf_synth= RandomForest(self.train_syth_x, self.train_syth_y, self.test_x)
        self.synth_rf_pred= self.rf_synth.train_predict()
        self.gb= GradientBoosting(self.train_x, self.train_y, self.test_x)
        self.gb_pred= self.gb.train_predict()
        self.gb_synth= GradientBoosting(self.train_syth_x, self.train_syth_y, self.test_x)
        self.synth_gb_pred= self.gb_synth.train_predict()
    
    def compare_perf(self):
        self.perf_comparaison['Logistical Regression']= {'f1 real':f1_score(self.test_y, np.where(self.lr_pred>=0.3,1,0)),
                                        'f1 synth':f1_score(self.test_y, np.where(self.synth_lr_pred>=0.3,1,0))
                                        , 'AUC ROC real': roc_auc_score(self.test_y, self.lr_pred)
                                        , 'AUC ROC synth': roc_auc_score(self.test_y, self.synth_lr_pred)}
        self.perf_comparaison['Tree']= {'f1 real':f1_score(self.test_y, np.where(self.tree_pred>=0.5,1,0)),
                                        'f1 synth':f1_score(self.test_y, np.where(self.synth_tree_pred>=0.5,1,0))
                                        , 'AUC ROC real': roc_auc_score(self.test_y, self.tree_pred)
                                        , 'AUC ROC synth': roc_auc_score(self.test_y, self.synth_tree_pred)}
        self.perf_comparaison['Random Forest']= {'f1 real':f1_score(self.test_y, np.where(self.rf_pred>=0.5,1,0)),
                                        'f1 synth':f1_score(self.test_y, np.where(self.synth_rf_pred>=0.5,1,0))
                                        , 'AUC ROC real': roc_auc_score(self.test_y, self.rf_pred)
                                        , 'AUC ROC synth': roc_auc_score(self.test_y, self.synth_rf_pred)}
        self.perf_comparaison['Gadient Boosting']= {'f1 real':f1_score(self.test_y, np.where(self.gb_pred>=0.3,1,0)),
                                        'f1 synth':f1_score(self.test_y, np.where(self.synth_gb_pred>=0.3,1,0))
                                        , 'AUC ROC real': roc_auc_score(self.test_y, self.gb_pred)
                                        , 'AUC ROC synth': roc_auc_score(self.test_y, self.synth_gb_pred)}
        self.perf_df= pd.DataFrame.from_dict(self.perf_comparaison, orient='index')
    
    def plot_perf(self):
        fig, ax= plt.subplots(2,2, figsize=(10,20))

        precision, recall, th= precision_recall_curve(self.test_y, self.lr_pred)
        ax[0,0].plot(recall, precision, label= 'real')
        precision, recall, th= precision_recall_curve(self.test_y, self.synth_lr_pred)
        ax[0,0].plot(recall,precision, label= 'synthetic')
        ax[0,0].set_title('Logistical Regression')
        ax[0,0].set_xlabel('recall')
        ax[0,0].set_ylabel('precision')

        precision, recall, th= precision_recall_curve(self.test_y, self.tree_pred)
        ax[0,1].plot(recall, precision, label= 'real')
        precision, recall, th= precision_recall_curve(self.test_y, self.synth_tree_pred)
        ax[0,1].plot(recall,precision, label= 'synthetic')
        ax[0,1].set_title('Classification Tree')
        ax[0,1].set_xlabel('recall')
        ax[0,1].set_ylabel('precision')

        precision, recall, th= precision_recall_curve(self.test_y, self.rf_pred)
        ax[1,0].plot(recall, precision, label= 'real')
        precision, recall, th= precision_recall_curve(self.test_y, self.synth_rf_pred)
        ax[1,0].plot(recall,precision, label= 'synthetic')
        ax[1,0].set_title('Random Forest')
        ax[1,0].set_xlabel('recall')
        ax[1,0].set_ylabel('precision')

        precision, recall, th= precision_recall_curve(self.test_y, self.gb_pred)
        ax[1,1].plot(recall, precision, label= 'real')
        precision, recall, th= precision_recall_curve(self.test_y, self.synth_gb_pred)
        ax[1,1].plot(recall,precision, label= 'synthetic')
        ax[1,1].set_title('Gradient Boosting')
        ax[1,1].set_xlabel('recall')
        ax[1,1].set_ylabel('precision')

        plt.legend()
        plt.show()
    
    def __call__(self):
        self.init_models()
        self.compare_perf()
        print(self.perf_df)
        self.plot_perf()



        
    

"""
dt= DecisionTree(train_x, train_y, test_x)
pred= dt.train_predict()
disp = plot_precision_recall_curve(dt.best_tree, test_x, test_y)
disp.ax_.set_title('Precision-Recall curve')


rf= RandomForest(train_x, train_y, test_x)
pred= rf.train_predict()
disp = plot_precision_recall_curve(rf.best_rf, test_x, test_y)
disp.ax_.set_title('Precision-Recall curve')
plt.plot()

gb= GradientBoosting(train_x, train_y, test_x)
pred= gb.train_predict()
disp = plot_precision_recall_curve(gb.best_gb, test_x, test_y)
disp.ax_.set_title('Precision-Recall curve')
plt.plot()
"""
