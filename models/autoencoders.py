import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data_utils
from torch.autograd import Variable
from torch.nn.parameter import Parameter
import torch.nn.functional as F
from utils.post_process import *
from attacks.shadow_membership import *
from attacks.collision_membership import *
from attacks.distance_membership import *
from opacus import PrivacyEngine
from opacus.utils.batch_memory_manager import BatchMemoryManager




device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')  # Slower with cpu 
#device = torch.device('cpu')
print(device)


class Encoder(nn.Module):
    def __init__(self, data_dim, encoder_dim):
        super().__init__()
        self.fc1= nn.Linear(data_dim, int(data_dim/2))
        self.r1=  nn.LeakyReLU(0.2 ,True)
        self.out_mu= nn.Linear(int(data_dim/2), encoder_dim)
        self.out_log_var= nn.Linear(int(data_dim/2), encoder_dim)
    
    def forward(self, x):
        x= self.r1(self.fc1(x))
        return self.out_mu(x), self.out_log_var(x)

class CEncoder(nn.Module):
    def __init__(self, data_dim, encoder_dim):
        super().__init__()
        self.fc1= nn.Linear(data_dim, int(data_dim/2))
        self.r1=  nn.LeakyReLU(0.2 ,True)
        self.out_mu= nn.Linear(int(data_dim/2), encoder_dim)
        self.out_log_var= nn.Linear(int(data_dim/2), encoder_dim)
    
    def forward(self, x, cond):
        x= torch.cat((x,cond), axis=1)
        x= self.r1(self.fc1(x))
        return self.out_mu(x), self.out_log_var(x)

class Decoder(nn.Module):
    def __init__(self, data_dim, encoder_dim):
        super().__init__()
        self.fc1= nn.Linear(encoder_dim, int(data_dim/2))
        self.r1=  nn.LeakyReLU(0.2 ,True)
        self.out= nn.Linear(int(data_dim/2), data_dim)
    
    def forward(self, x):
        x= self.r1(self.fc1(x))
        return self.out(x)

class CDecoder(nn.Module):
    def __init__(self, data_dim, encoder_dim):
        super().__init__()
        self.fc1= nn.Linear(encoder_dim, int(data_dim/2))
        self.r1=  nn.LeakyReLU(0.2 ,True)
        self.out= nn.Linear(int(data_dim/2), data_dim-1)
        self.out_cond= nn.Linear(int(data_dim/2), 1)
        self.sig= nn.Sigmoid()
    
    def forward(self, x):
        x= self.r1(self.fc1(x))
        return self.out(x), self.sig(self.out_cond(x))

class CDecoder2(nn.Module):
    def __init__(self, data_dim, encoder_dim):
        super().__init__()
        self.fc1= nn.Linear(encoder_dim+1, int(data_dim/2))
        self.r1=  nn.LeakyReLU(0.2 ,True)
        self.out= nn.Linear(int(data_dim/2), data_dim-1)
        self.out_cond= nn.Linear(int(data_dim/2), 1)
        self.sig= nn.Sigmoid()
    
    def forward(self, x, cond):
        x= torch.concat((x, cond), axis=1)
        x= self.r1(self.fc1(x))
        return self.out(x), self.sig(self.out_cond(x))

class CDecoder2Dropout(nn.Module):
    def __init__(self, data_dim, encoder_dim):
        super().__init__()
        self.fc1= nn.Linear(encoder_dim+1, int(data_dim/2))
        self.r1=  nn.LeakyReLU(0.2 ,True)
        self.out= nn.Linear(int(data_dim/2), data_dim-1)
        self.out_cond= nn.Linear(int(data_dim/2), 1)
        self.sig= nn.Sigmoid()
        self.dropout= nn.Dropout(.5)
    
    def forward(self, x, cond):
        x= torch.concat((x, cond), axis=1)
        x= self.dropout(self.r1(self.fc1(x)))
        return self.out(x), self.sig(self.out_cond(x))


class CEncoderDropout(nn.Module):
    def __init__(self, data_dim, encoder_dim):
        super().__init__()
        self.fc1= nn.Linear(data_dim, int(data_dim/2))
        self.r1=  nn.LeakyReLU(0.2 ,True)
        self.out_mu= nn.Linear(int(data_dim/2), encoder_dim)
        self.out_log_var= nn.Linear(int(data_dim/2), encoder_dim)
        self.dropout= nn.Dropout(.5)

    
    def forward(self, x, cond):
        x= torch.cat((x,cond), axis=1)
        x= self.dropout(x)
        x= self.dropout(self.r1(self.fc1(x)))
        return self.out_mu(x), self.out_log_var(x)

class VAE(nn.Module):
    def __init__(self, data_dim, lr, encoder_dim='auto'):
        super().__init__()
        self.data_dim= data_dim
        if encoder_dim == 'auto':
            self.encoder_dim= int(data_dim/4)
        else:
            self.encoder_dim= encoder_dim
        self.lr= lr
        self.encoder= Encoder(data_dim, self.encoder_dim)
        self.decoder= Decoder(data_dim, self.encoder_dim)
        self.encoder.to(device)
        self.decoder.to(device)
    
    def reparameterize(self, mu, log_var):
        """
        :param mu: mean from the encoder's latent space
        :param log_var: log variance from the encoder's latent space
        """
        std = torch.exp(0.5*log_var) # standard deviation
        eps = torch.randn_like(std) # `randn_like` as we need the same size
        sample = mu + (eps * std) # sampling as if coming from the input space
        return sample
    
    def forward(self, x):
        mu, log_var= self.encoder(x)
        z= self.reparameterize(mu, log_var)
        reconstruction= self.decoder(z)
        return reconstruction, mu, log_var
    
    def fit(self, x_train,batch_size, epochs):
        self.losses=[]
        self.optimizer= optim.Adam(self.parameters(), lr=self.lr)
        data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(x_train)),
                                            batch_size=batch_size, shuffle=True, drop_last=True)
        # For each epoch
        for epoch in range(epochs):
            # For each batch in the dataloader
            running_loss=0.
            for i, data in enumerate(data_loader, 0):
                self.optimizer.zero_grad()
                data= data[0].to(device)
                out, mu, log_var = self.forward(data)
                kl= -0.5 * torch.sum(1 + log_var - mu.pow(2) - log_var.exp())
                mse= ((out- data)**2).sum()
                loss= mse+ kl
                running_loss += loss.item()/len(data_loader.dataset)
                loss.backward()
                self.optimizer.step()
            print('{}/{} loss: {}'.format(epoch, epochs, running_loss))
            self.losses.append(running_loss)
    
    def generate_fake(self, synth_size):
        latent= torch.randn((synth_size, self.encoder_dim), device= device)
        return self.decoder(latent)

class CVAE(VAE):
    def __init__(self, data_dim, lr, encoder_dim='auto'):
        super().__init__(data_dim, lr)
        self.data_dim= data_dim
        if encoder_dim == 'auto':
            self.encoder_dim= int(data_dim/4)
        else:
            self.encoder_dim= encoder_dim
        self.lr= lr
        self.encoder= CEncoder(self.data_dim, self.encoder_dim)
        self.decoder= CDecoder(self.data_dim, self.encoder_dim)
        self.encoder.to(device)
        self.decoder.to(device)
    
    def forward(self, x, cond):
        mu, log_var= self.encoder(x, cond)
        z= self.reparameterize(mu, log_var)
        reconstruction, pred= self.decoder(z)
        return reconstruction, pred,mu, log_var
    
    def fit(self, x_train, cond,batch_size, epochs):
        self.losses=[]
        criterion = nn.BCELoss(reduction='sum')
        self.optimizer= optim.Adam(self.parameters(), lr=self.lr)
        data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(x_train), torch.FloatTensor(cond).reshape(-1,1)),
                                            batch_size=batch_size, shuffle=True, drop_last=True)
        # For each epoch
        for epoch in range(epochs):
            # For each batch in the dataloader
            running_loss=0.
            for i, data in enumerate(data_loader, 0):
                self.optimizer.zero_grad()
                cond= data[1].to(device)
                data= data[0].to(device)
                out, pred, mu, log_var = self.forward(data, cond)
                kl= -0.5 * torch.sum(1 + log_var - mu.pow(2) - log_var.exp())
                mse= ((out- data)**2).sum()
                bce= criterion(pred, cond)
                loss= mse+ kl + bce * 2
                running_loss += loss.item()/len(data_loader.dataset)
                loss.backward()
                self.optimizer.step()
            print('{}/{} loss: {}'.format(epoch, epochs, running_loss))
            self.losses.append(running_loss)
    
    def generate_fake(self, synth_size):
        latent= torch.randn((synth_size, self.encoder_dim), device= device)
        out, pred= self.decoder(latent)
        return torch.cat((out, pred), axis=1)

class CVAE2(CVAE):
    def __init__(self, data_dim, lr, encoder_dim='auto'):
        super().__init__(data_dim, lr)
        self.data_dim= data_dim
        if encoder_dim == 'auto':
            self.encoder_dim= int(data_dim/4)
        else:
            self.encoder_dim= encoder_dim
        self.lr= lr
        self.encoder= CEncoder(self.data_dim, self.encoder_dim)
        self.decoder= CDecoder2(self.data_dim, self.encoder_dim)
        self.encoder.to(device)
        self.decoder.to(device)
    
    def forward(self, x, cond):
        mu, log_var= self.encoder(x, cond)
        z= self.reparameterize(mu, log_var)
        reconstruction, pred= self.decoder(z, cond)
        return reconstruction, pred,mu, log_var

    def generate_fake(self, cond):
        latent= torch.randn((cond.shape[0], self.encoder_dim), device= device)
        out, pred= self.decoder(latent, torch.FloatTensor(cond).view(-1,1).to(device))
        return torch.cat((out, pred), axis=1)

class CVAE2Dropout(CVAE2):
    def __init__(self, data_dim, lr, encoder_dim='auto'):
        super().__init__(data_dim, lr, encoder_dim)
        self.encoder= CEncoderDropout(self.data_dim, self.encoder_dim)
        self.decoder= CDecoder2Dropout(self.data_dim, self.encoder_dim)
        self.encoder.to(device)
        self.decoder.to(device)

class CVAE2DP(CVAE2):
    def __init__(self, data_dim, lr, epsilon, delta, grad_norm, encoder_dim='auto'):
        super().__init__(data_dim, lr, encoder_dim)
        self.epsilon= epsilon
        self.delta= delta
        self.grad_norm= grad_norm

    def fit(self, x_train, cond,batch_size, epochs):
        self.losses=[]
        criterion = nn.BCELoss(reduction='sum')
        self.optimizer= optim.Adam(self.parameters(), lr=self.lr)
        data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(x_train), torch.FloatTensor(cond).reshape(-1,1)),
                                            batch_size=batch_size, shuffle=True, drop_last=False)
       
        self.privacy_engine = PrivacyEngine()
        self.model, self.optimizer, data_loader = self.privacy_engine.make_private_with_epsilon(
            module=self,
            optimizer=self.optimizer,
            data_loader=data_loader,
            target_delta=self.delta,
            target_epsilon=self.epsilon, 
            epochs=epochs,
            max_grad_norm=self.grad_norm,
        )
        # For each epoch
        with BatchMemoryManager(
        data_loader=data_loader, 
        max_physical_batch_size=64, 
        optimizer=self.optimizer
        ) as memory_safe_data_loader:
            for epoch in range(epochs):
                # For each batch in the dataloader
                running_loss=0.
                for i, data in enumerate(memory_safe_data_loader, 0):
                    self.optimizer.zero_grad()
                    cond= data[1].to(device)
                    data= data[0].to(device)
                    out, pred, mu, log_var = self.forward(data, cond)
                    kl= -0.5 * torch.sum(1 + log_var - mu.pow(2) - log_var.exp())
                    mse= ((out- data)**2).sum()
                    bce= criterion(pred, cond)
                    loss= mse+ kl + bce * 5
                    running_loss += loss.item()/len(data_loader.dataset)
                    loss.backward()
                    self.optimizer.step()
                print('{}/{} loss: {}, epsilon: {}'.format(epoch, epochs, running_loss, self.privacy_engine.get_epsilon(self.delta)))
                self.losses.append(running_loss)
    
        


if __name__=='__main__':
    vae= CVAE2(real.shape[1], 0.0001)
    vae.fit(real.drop(TARGET, axis=1).values, real[TARGET].values,64,300)
    fake= vae.generate_fake(real[TARGET].values)

    fake= pd.DataFrame(fake.detach().cpu().numpy(), columns= real.columns)
    #fake= pd.DataFrame(fake.detach().cpu().numpy(), columns= real.columns)
    #int_col= real_x.columns.tolist()[:continuous_dim]
    #int_col= real.columns[:continuous_dim].tolist()
    #int_col.append(TARGET)
    int_col= real.columns
    #int_col= ['age', 'NumberOfTime3059DaysPastDueNotWorse', 'NumberOfOpenCreditLinesAndLoans', 'NumberOfTimes90DaysLate', 'NumberRealEstateLoansOrLines', 'NumberOfTime6089DaysPastDueNotWorse', 'NumberOfDependents', TARGET]
    for col in int_col:
        fake[col]= transform_to_ref_values(fake, real, col, col)

    cols= fake.columns
    fig, ax= plt.subplots(int(len(real.columns)/2)+1,2, figsize=(10,120))
    z=0   
    for i in range(int(len(real.columns)/2)+1):
        for j in range(2):
            ax[i,j].hist(real[cols[z]], label= 'real', bins=100, cumulative=True, histtype='step')
            ax[i,j].hist(fake[cols[z]], label= 'fake', bins=100, cumulative=True, histtype='step')
            ax[i,j].set_title(cols[z])
            z+=1
            if z>= len(cols)-1:
                break
    plt.legend()
    plt.show()

    mc= ModelComparaison(real.drop(TARGET, axis=1), real[TARGET], test_x, test_y, fake.drop(TARGET, axis=1), fake[TARGET])
    #mc= ModelComparaison(real_x, real_y, test_x, test_y, fake.drop('over_50k', axis=1), fake['over_50k'])
    mc()

    def print_membership_results(test):
        print('confusion matrix:',confusion_matrix(test.is_member, test.pred))
        print('precision',precision_score(test.is_member, test.pred))
        print('recall',recall_score(test.is_member, test.pred))
        print('f1:',f1_score(test.is_member, test.pred))
        print('accuracy', accuracy_score(test.is_member, test.pred))

    print('\nGAN Leak')
    test=gan_leak_attack(real.sample(n= test_membership.shape[0], random_state=42),test_membership, fake,.5)
    print_membership_results(test)
    threshold= test.score.quantile(.01)
    test['pred']= np.where(test.score<= threshold,1,0)
    print('############### 1%')
    print_membership_results(test)
    #vizualize_membership(test, 'pca',2)
    print('\n MC attack')
    test_mc=monte_carlo_attack(real.sample(n= test_membership.shape[0], random_state=42),test_membership, fake,.5)
    print_membership_results(test_mc)
    threshold= test_mc.score.quantile(.99)
    test_mc['pred']= np.where(test_mc.score>= threshold,1,0)
    print('############### 1%')
    print_membership_results(test_mc)
    #vizualize_membership(test, 'tsne', 2)
    print('\nLOGAN')
    test_logan= logan_attack_vae(real.sample(n= test_membership.shape[0], random_state=42),test_membership, fake,.5,TARGET, 100)
    print_membership_results(test_logan)
    threshold= test_logan.score.quantile(.01)
    test_logan['pred']= np.where(test_logan.score<= threshold,1,0)
    print('############### 1%')
    print_membership_results(test_logan)
    #vizualize_membership(test, 'tsne', 2)
    print('\nTable Gan')
    test_table= table_gan_attack_vae(real.sample(n= test_membership.shape[0], random_state=42),test_membership, fake,.5, TARGET, 100)
    print_membership_results(test_table)
    threshold= test_table.score.quantile(.99)
    test_table['pred']= np.where(test_table.score>= threshold,1,0)
    print('############### 1%')
    print_membership_results(test_table)
    #vizualize_membership(test, 'tsne', 2)

    print('collision attack')
    synth, rf=collision_attack_vae(real, fake, TARGET, 500, int_col)

    precision, recall, th= precision_recall_curve(synth.collision, synth.score)
    plt.figure()
    plt.plot(recall, precision, label= 'real')
    plt.title('Gradient Boosting')
    plt.xlabel('recall')
    plt.ylabel('precision') 
    plt.show()
    print('f1:', f1_score(synth.collision, np.where(synth.score>=.5,1,0)))
    print('precision:', precision_score(synth.collision, np.where(synth.score>=.5,1,0)))
    print('recall:', recall_score(synth.collision, np.where(synth.score>=.5,1,0)))
