from tkinter.tix import Tree
import pandas as pd
from models.gans import *
from sklearn.model_selection import train_test_split
import numpy as np
from models.classifier import *
from sklearn.metrics import confusion_matrix, precision_score, recall_score, f1_score, accuracy_score
from utils.pre_process_tab import find_cat_len
import tqdm
import glob
import seaborn as sns
from sklearn import tree
from utils.viz import *
from utils.investigate_set import *
from sklearn.preprocessing import OneHotEncoder

def ohe(train, synth, synth2, ohe_col):
    ohe= OneHotEncoder(sparse=False, handle_unknown='ignore')
    ohe.fit(train[ohe_col])
    train_ohe= ohe.transform(train[ohe_col])
    synth_ohe= ohe.transform(synth[ohe_col])
    synth_ohe2= ohe.transform(synth2[ohe_col])
    train= pd.concat([train[[col for col in train.columns if col not in ohe_col]].reset_index(drop=True),
                    pd.DataFrame(train_ohe, columns= ohe.get_feature_names())], axis=1)
    synth= pd.concat([synth[[col for col in synth.columns if col not in ohe_col]].reset_index(drop=True),
                    pd.DataFrame(synth_ohe, columns= ohe.get_feature_names())], axis=1)
    synth2= pd.concat([synth2[[col for col in synth2.columns if col not in ohe_col]].reset_index(drop=True),
                    pd.DataFrame(synth_ohe2, columns= ohe.get_feature_names())], axis=1)
    return train, synth, synth2

def compute_row_frequency(df):
    df['freq']= df.duplicated(keep=False)
    df= df.groupby(df.columns.tolist(), as_index=False)['freq'].sum()
    return df

def compute_row_frequency_safe(df):
    freq=[]
    df['freq']= df.duplicated(keep=False)
    for _, row in tqdm( df.iterrows(), total=df.shape[0]):
        if row['freq']==True:
            c=df.apply(lambda r: 1 if sum(r==row)==10 else 0, axis=1)
            freq.append(sum(c))
        else:
            freq.append(1)
    df['freq']=freq
    return df

def compute_row_frequency(df):
    df['freq']= df.duplicated(keep=False)
    df_grp= df.groupby(df.columns.tolist(), as_index=False)['freq'].sum()
    return pd.merge(df.drop('freq', axis=1), df_grp, on= df.drop('freq', axis=1).columns.tolist(), how='left')

def find_overlap(real, synth):
    synth_full= pd.merge(synth, real, on= synth.columns.to_list(), indicator='Exist', how='left')
    synth_full['collision']= np.where(synth_full['Exist']=='both',1,0)
    synth= pd.merge(synth, synth_full.drop_duplicates(), on=real.columns.tolist(), how='left')
    return synth['collision']

def collision_membership_no_train(train, synth_list, synth2_list):
    big_synth=[]
    big_synth2=[]
    for i in range(len(synth_list)):
        synth= synth_list[i]
        #synth= synth.loc[synth.COW!=8, :]
        synth= synth[train.columns.tolist()]
        synth2= synth2_list[i]
        #synth2= synth2.loc[synth2.COW!=8, :]
        synth2= synth2[train.columns.tolist()]
        synth= compute_row_frequency(synth)
        synth2= compute_row_frequency(synth2)
        synth2['collision']= find_overlap(synth.drop('freq', axis=1), synth2.drop('freq', axis=1))
        synth['collision']= find_overlap(train, synth.drop('freq', axis=1))
        big_synth.append(synth.reset_index(drop=True))
        big_synth2.append(synth2.reset_index(drop=True))
    synth= pd.concat(big_synth, axis=0, ignore_index=True)
    synth2= pd.concat(big_synth2, axis=0, ignore_index=True)
    #train, synth, synth2= ohe(train, synth, synth2,train.columns.tolist())
    # train classifier
    print('train rf')
    rf= RandomForest(synth2.drop(['collision'], axis=1), synth2['collision'], synth.drop('collision', axis=1))
    synth['score']= rf.train_predict()
    return synth, rf

#if __name__=='__main__':
train= pd.read_csv('D:/data/datasets/datasets/employment/Employment_ori.csv').drop(columns=['AGEP'])
all_synth_path= glob.glob('D:/data/datasets/datasets/employment/MST/esp10/synth/*.csv')
all_synth_path.sort()
synth_list=[]
for p in all_synth_path:
    print(p)
    synth_list.append(pd.read_csv(p))
all_synth2_path= glob.glob('D:/data/datasets/datasets/employment/MST/esp10/synth synth/*.csv')
all_synth2_path.sort()
synth2_list=[]
for p in all_synth2_path:
    print(p)
    synth2_list.append(pd.read_csv(p))

#synth_list= [pd.read_csv('/home/hadrien/Downloads/data collisions/20k/synth_epsi1.csv')]
#synth2_list= [pd.read_csv('/home/hadrien/Downloads/data collisions/20k/synth_synth_epsi1_1.csv')]

synth, rf=collision_membership_no_train(train, synth_list, synth2_list)

precision, recall, th= precision_recall_curve(synth.collision, synth.score)
plt.figure()
plt.plot(recall, precision, label= 'real')
plt.title('Gradient Boosting')
plt.xlabel('recall')
plt.ylabel('precision')
plt.show()

print('f1:', f1_score(synth.collision, np.where(synth.score>=.5,1,0)))
print('precision:', precision_score(synth.collision, np.where(synth.score>=.5,1,0)))
print('recall:', recall_score(synth.collision, np.where(synth.score>=.5,1,0)))

"""
f1: 0.6308150689569556
precision: 0.5607343499197431
recall: 0.7209152349997421
"""


# sorted(zip(clf.feature_importances_, X.columns), reverse=True)
feature_imp = pd.DataFrame(sorted(zip(rf.best_rf.feature_importances_,synth.drop(columns=['score', 'collision']).columns)), columns=['Value','Feature'])

plt.figure()
sns.barplot(x="Value", y="Feature", data=feature_imp.sort_values(by="Value", ascending=False))
plt.title('Classifier feature imp')
plt.show()

#analyse frequency of reid obs from train
#reid= synth.loc[(synth.collision==1)&(synth.score>=.5), :]
#reid= synth.loc[(synth.collision==1), :]
#reid= reid.drop(['freq', 'score'], axis=1).drop_duplicates()
#reid.shape # 2934 observations uniques retrouv/es depuis train
#train_reid= pd.merge(train, reid, on= train.columns.tolist(), how='inner')
#train_reid= compute_row_frequency(train_reid)
#train_reid.freq.hist(bins=50)

#synth_viz=synth.rename(columns={'collision':'is_member'})
#synth_viz['pred']= np.where(synth_viz.score >=0.5,1,0)
#vizualize_membership(synth_viz, 'tsne', 2)

dist_analysis(synth.drop(columns=['score', 'freq']), 'collision', 20)
"""
collision avg dist: 0.04551903579075836 collision median_dist: 0.022053894293983838
avg dist unique: 0.3156214310921747 unique median_dist: 0.23379605218644053
"""
#isolation_forest_analysis(synth.drop(columns=['score', 'freq']), 'collision')
"""
collision avg isolation score: -0.39628110999238014 collision median isolation score: -0.3916716465082283
avg isolation score unique: -0.4097993725868411 unique median isolation score: -0.40804374691765655
"""
#tree.plot_tree(rf.best_tree)