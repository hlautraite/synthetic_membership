import pandas as pd
from models.gans import *
from sklearn.model_selection import train_test_split
import numpy as np
from models.classifier import *
from sklearn.metrics import confusion_matrix, precision_score, recall_score, f1_score, accuracy_score
from utils.pre_process_tab import find_cat_len
from models.autoencoders import *
from utils.post_process import *



def create_test_set(training_samples, non_training_samples):
    training_samples['is_member']=1
    non_training_samples['is_member']=0
    test= pd.concat([training_samples.reset_index(drop=True), non_training_samples.reset_index(drop=True)], axis=0)
    return test

def score_c_disc(feat, cond, gan):
    data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(feat.values), torch.FloatTensor(cond.values).reshape(-1,1)),
                                            batch_size=64, shuffle=False, drop_last=False)
    pred=np.array([])
    for i, data in enumerate(data_loader, 0):
        score= gan.disc.forward(data[0].to(device), data[1].to(device))
        pred= np.concatenate([pred, score.view(-1).detach().cpu().numpy()])
    return pred


def score_ac_disc(feat, cond, gan):
    data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(feat.values)),
                                            batch_size=64, shuffle=False, drop_last=False)
    pred=np.array([])
    for i, data in enumerate(data_loader, 0):
        score,_= gan.disc.forward(data[0].to(device))
        pred= np.concatenate([pred, score.view(-1).detach().cpu().numpy()])
    return pred

def score_vae(df, cond, vae):
    criterion = nn.BCELoss(reduction='none')
    reconstruction, pred,mu, log_var= vae.forward(torch.FloatTensor(df.drop([cond,'is_member'] ,axis=1).values).to(device), torch.FloatTensor(df[cond].values).reshape(-1,1).to(device))
    #kl= -0.5 * torch.sum(1 + log_var - mu.pow(2) - log_var.exp())
    mse= ((reconstruction- torch.FloatTensor(df.drop([cond,'is_member'] ,axis=1).values).to(device))**2)
    bce= criterion(pred, torch.FloatTensor(df[cond].values).reshape(-1,1).to(device))
    loss= mse + bce
    return torch.concat((loss.sum(axis=1).reshape(-1,1), mu, log_var), axis=1).detach().cpu().numpy()


def logan_attack_ac(training_samples, non_training_samples, synth,pct_real, cond):
    test= create_test_set(training_samples, non_training_samples)
    gan= ACGan(64,107,.0001, 5)
    gan.fit(synth.drop(cond, axis=1).values, synth[cond].values, 64, 100)
    end= test.shape[0]%64
    if end!=0:
        test= test.iloc[:-end,:]
    data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(test.drop([cond,'is_member'] ,axis=1).values)),
                                            batch_size=64, shuffle=False, drop_last=True)
    pred=np.array([])
    for i, data in enumerate(data_loader, 0):
        score, _= gan.disc.forward(data[0].to(device))
        pred= np.concatenate([pred, score.view(-1).detach().cpu().numpy()])
    test['score']= pred
    threshold= test['score'].quantile(pct_real)
    test['pred']= np.where(test.score >= threshold,1,0)
    return test

def logan_attack_c(training_samples, non_training_samples, synth,pct_real, cond, epochs=100):
    test= create_test_set(training_samples, non_training_samples)
    gan= WCGan(64,synth.drop(cond, axis=1).shape[1],.000001, 3)
    gan.fit(synth.drop(cond, axis=1).values, synth[cond].values, 128, epochs)
    end= test.shape[0]%64
    if end!=0:
        test= test.iloc[:-end,:]
    data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(test.drop([cond,'is_member'] ,axis=1).values), torch.FloatTensor(test[cond].values).reshape(-1,1)),
                                            batch_size=64, shuffle=False, drop_last=False)
    pred=np.array([])
    for i, data in enumerate(data_loader, 0):
        score= gan.disc.forward(data[0].to(device), data[1].to(device))
        pred= np.concatenate([pred, score.view(-1).detach().cpu().numpy()])    
    test['score']= pred
    threshold= test['score'].quantile(pct_real)
    test['pred']= np.where(test.score >= threshold,1,0)
    return test

def logan_attack_c_cat(training_samples, non_training_samples, synth,pct_real, cond):
    test= create_test_set(training_samples, non_training_samples)
    ohe_dim= list(find_cat_len(synth))
    continuous_dim= len(synth.columns)- sum(ohe_dim) -1# -1 pour la colonne target
    gan= WCGanCat(64, continuous_dim, ohe_dim, 0.000005)
    gan.fit(synth.drop(cond, axis=1).values, synth[cond].values, 256, 500)
    end= test.shape[0]%64
    if end!=0:
        test= test.iloc[:-end,:]
    data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(test.drop([cond,'is_member'] ,axis=1).values), torch.FloatTensor(test[cond].values).reshape(-1,1)),
                                            batch_size=64, shuffle=False, drop_last=False)
    pred=np.array([])
    for i, data in enumerate(data_loader, 0):
        score= gan.disc.forward(data[0].to(device), data[1].to(device))
        pred= np.concatenate([pred, score.view(-1).detach().cpu().numpy()])    
    test['score']= pred
    threshold= test['score'].quantile(pct_real)
    test['pred']= np.where(test.score >= threshold,1,0)
    return test

def logan_attack_ac_cat(training_samples, non_training_samples, synth,pct_real, cond):
    test= create_test_set(training_samples, non_training_samples)
    ohe_dim= list(find_cat_len(synth))
    continuous_dim= len(synth.columns)- sum(ohe_dim) -1# -1 pour la colonne target
    gan= ACGanCat(64, continuous_dim, ohe_dim, 0.000005)
    gan.fit(synth.drop(cond, axis=1).values, synth[cond].values, 64, 500)
    end= test.shape[0]%64
    if end!=0:
        test= test.iloc[:-end,:]
    data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(test.drop([cond,'is_member'] ,axis=1).values), torch.FloatTensor(test[cond].values).reshape(-1,1)),
                                            batch_size=64, shuffle=False, drop_last=False)
    test['score']=score_ac_disc(test.drop([cond, 'is_member'] ,axis=1), test[cond], gan)
    threshold= test['score'].quantile(pct_real)
    test['pred']= np.where(test.score >= threshold,1,0)
    return test

def logan_attack_vae(training_samples, non_training_samples, synth,pct_real, cond, epochs=100):
    criterion = nn.BCELoss(reduction='none')
    test= create_test_set(training_samples, non_training_samples)
    ohe_dim= list(find_cat_len(synth))
    continuous_dim= len(synth.columns)- sum(ohe_dim) -1# -1 pour la colonne target
    vae= CVAE2(synth.shape[1], 0.0001, 4)
    vae.fit(synth.drop(cond, axis=1).values, synth[cond].values,64, epochs)
    reconstruction, pred,mu, log_var= vae.forward(torch.FloatTensor(test.drop([cond,'is_member'] ,axis=1).values).to(device), torch.FloatTensor(test[cond].values).reshape(-1,1).to(device))
    #kl= -0.5 * torch.sum(1 + log_var - mu.pow(2) - log_var.exp())
    mse= ((reconstruction- torch.FloatTensor(test.drop([cond,'is_member'] ,axis=1).values).to(device))**2)
    bce= criterion(pred, torch.FloatTensor(test[cond].values).reshape(-1,1).to(device))
    loss= mse + bce #+kl
    test['score']= loss.sum(axis=1).detach().cpu().numpy()
    threshold= test['score'].quantile(pct_real)
    test['pred']= np.where(test.score <= threshold,1,0)
    return test


def table_gan_attack_c(training_samples, non_training_samples, synth,pct_real, cond, epochs=100):
    test= create_test_set(training_samples, non_training_samples)
    train_synth, test_synth= train_test_split(synth, train_size=0.8, random_state=42)
    # train shadow gan on 80% of the synth data
    gan= WCGan(64,synth.drop(cond, axis=1).shape[1],.000005, 3)
    gan.fit(train_synth.drop(cond, axis=1).values, train_synth[cond].values, 128, epochs)
    #generate fake fake data
    end=test_synth.shape[0]%64
    if end!=0:
        test_synth= test_synth.iloc[:-end,:]
    train_synth_sample= train_synth.sample(n=test_synth.shape[0], random_state=0)
    member_train= create_test_set(train_synth_sample, test_synth)
    # score synth data with the discriminator
    member_train['score']= score_c_disc(member_train.drop([cond, 'is_member'] ,axis=1), member_train[cond], gan)
    end= test.shape[0]%64
    if end!=0:
        test= test.iloc[:-end,:]
    test['score']=score_c_disc(test.drop([cond, 'is_member'] ,axis=1), test[cond], gan)
    # train classifier
    rf= RandomForest(member_train.drop('is_member', axis=1), member_train['is_member'], test.drop(columns=['is_member']))
    test['score']= rf.train_predict()
    threshold= test['score'].quantile(pct_real)
    test['pred']= np.where(test.score >= threshold,1,0)
    return test

def table_gan_attack_c_cat(training_samples, non_training_samples, synth,pct_real, cond):
    test= create_test_set(training_samples, non_training_samples)
    ohe_dim= list(find_cat_len(synth))
    continuous_dim= len(synth.columns)- sum(ohe_dim) -1 # -1 pour la colonne target
    train_synth, test_synth= train_test_split(synth, train_size=0.8, random_state=42)
    # train shadow gan on 80% of the synth data
    gan= WCGanCat(64, continuous_dim, ohe_dim, 0.00001)
    gan.fit(train_synth.drop(cond, axis=1).values, train_synth[cond].values, 256, 250)
    #generate fake fake data
    end=test_synth.shape[0]%64
    if end!=0:
        test_synth= test_synth.iloc[:-end,:]
    train_synth_sample= train_synth.sample(n=test_synth.shape[0], random_state=0)
    member_train= create_test_set(train_synth_sample, test_synth)
    # score synth data with the discriminator
    member_train['score']= score_c_disc(member_train.drop([cond, 'is_member'] ,axis=1), member_train[cond], gan)
    end= test.shape[0]%64
    if end!=0:
        test= test.iloc[:-end,:]
    test['score']=score_c_disc(test.drop([cond, 'is_member'] ,axis=1), test[cond], gan)
    # train classifier
    rf= RandomForest(member_train.drop('is_member', axis=1), member_train['is_member'], test.drop(columns=['is_member']))
    test['score']= rf.train_predict()
    threshold= test['score'].quantile(pct_real)
    test['pred']= np.where(test.score >= threshold,1,0)
    return test

def table_gan_attack_ac_cat(training_samples, non_training_samples, synth,pct_real, cond):
    test= create_test_set(training_samples, non_training_samples)
    ohe_dim= list(find_cat_len(synth))
    continuous_dim= len(synth.columns)- sum(ohe_dim) -1 # -1 pour la colonne target
    train_synth, test_synth= train_test_split(synth, train_size=0.8, random_state=42)
    # train shadow gan on 80% of the synth data
    gan= ACGanCat(64, continuous_dim, ohe_dim, 0.00001)
    gan.fit(train_synth.drop(cond, axis=1).values, train_synth[cond].values, 256, 250)
    #generate fake fake data
    end=test_synth.shape[0]%64
    if end!=0:
        test_synth= test_synth.iloc[:-end,:]
    train_synth_sample= train_synth.sample(n=test_synth.shape[0], random_state=0)
    member_train= create_test_set(train_synth_sample, test_synth)
    # score synth data with the discriminator
    member_train['score']= score_ac_disc(member_train.drop([cond, 'is_member'] ,axis=1), member_train[cond], gan)
    end= test.shape[0]%64
    if end!=0:
        test= test.iloc[:-end,:]
    test['score_']=score_ac_disc(test.drop([cond, 'is_member'] ,axis=1), test[cond], gan)
    # train classifier
    rf= RandomForest(member_train.drop('is_member', axis=1), member_train['is_member'], test.drop(columns=['is_member']))
    test['score']= rf.train_predict()
    threshold= test['score'].quantile(pct_real)
    test['pred']= np.where(test.score >= threshold,1,0)
    return test

def table_gan_attack_vae(training_samples, non_training_samples, synth,pct_real, cond, epochs):
    test= create_test_set(training_samples, non_training_samples)
    ohe_dim= list(find_cat_len(synth))
    continuous_dim= len(synth.columns)- sum(ohe_dim) -1 # -1 pour la colonne target
    train_synth, test_synth= train_test_split(synth, train_size=0.8, random_state=42)
    # train shadow gan on 80% of the synth data
    vae= CVAE2(synth.shape[1], 0.0001, 4)
    vae.fit(synth.drop(cond, axis=1).values, synth[cond].values,64, epochs)
    # score synth data with the discriminator
    train_synth_sample= train_synth.sample(n=test_synth.shape[0], random_state=0)
    member_train= create_test_set(train_synth_sample, test_synth)
    vae_scores= score_vae(member_train, cond, vae)
    member_train= pd.concat([member_train.reset_index(drop=True), pd.DataFrame(vae_scores)], axis=1)
    vae_scores= score_vae(test, cond, vae)
    test= pd.concat([test.reset_index(drop=True), pd.DataFrame(vae_scores)], axis=1)
    # train classifier
    rf= RandomForest(member_train.drop('is_member', axis=1), member_train['is_member'], test.drop(columns=['is_member']))
    test['score']= rf.train_predict()
    threshold= test['score'].quantile(pct_real)
    test['pred']= np.where(test.score >= threshold,1,0) 
    return test
