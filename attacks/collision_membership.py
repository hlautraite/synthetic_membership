from tkinter.tix import Tree
import pandas as pd
from models.gans import *
from sklearn.model_selection import train_test_split
import numpy as np
from models.classifier import *
from sklearn.metrics import confusion_matrix, precision_score, recall_score, f1_score, accuracy_score
from utils.pre_process_tab import find_cat_len
import tqdm
import glob
import seaborn as sns
from sklearn import tree
from utils.viz import *
from utils.post_process import *
from attacks.distance_membership import *
from models.autoencoders import *


def compute_row_frequency(df):
    df['freq']= df.duplicated(keep=False)
    df_grp= df.groupby(df.columns.tolist(), as_index=False)['freq'].sum()
    return pd.merge(df.drop('freq', axis=1), df_grp, on= df.drop('freq', axis=1).columns.tolist(), how='left')


def find_overlap(real, synth):
    synth_full= pd.merge(synth, real, on= synth.columns.to_list(), indicator='Exist', how='left')
    synth_full['collision']= np.where(synth_full['Exist']=='both',1,0)
    synth= pd.merge(synth, synth_full.drop_duplicates(), on=real.columns.tolist(), how='left')
    return synth['collision']

def create_test_set(training_samples, non_training_samples):
    training_samples['is_member']=1
    non_training_samples['is_member']=0
    test= pd.concat([training_samples.reset_index(drop=True), non_training_samples.reset_index(drop=True)], axis=0)
    return test

def score_c_disc(feat, cond, gan):
    data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(feat.values), torch.FloatTensor(cond.values).reshape(-1,1)),
                                            batch_size=64, shuffle=False, drop_last=False)
    pred=np.array([])
    for i, data in enumerate(data_loader, 0):
        score= gan.disc.forward(data[0], data[1])
        pred= np.concatenate([pred, score.view(-1).detach().numpy()])
    return pred

def find_aprox_overlap(train, synth):
    if synth.shape[1]>=20:
        pca= PCA(n_components=10)
        pca.fit(train)
        train_pca= pca.transform(train)
        synth_pca= pca.transform(synth)
        nearest_dist_train= find_nearest_neighbor_dist(train_pca, train_pca,2)
    else:
        nearest_dist_train= find_nearest_neighbor_dist(train, train,2)
    threshold= pd.Series(nearest_dist_train[:,1]).quantile(.15)
    print('threshold', threshold)
    if synth.shape[1]>=20:
        synth['distance_to_closest_real']= find_nearest_neighbor_dist(train_pca, synth_pca)
    else:
        synth['distance_to_closest_real']= find_nearest_neighbor_dist(train, synth)
    print('distance_to_closest_real', synth['distance_to_closest_real'].describe())
    synth['collision']= np.where(synth.distance_to_closest_real<= threshold,1,0)
    return synth.drop('distance_to_closest_real', axis=1)

def score_ac_disc(feat, cond, gan):
    data_loader = data_utils.DataLoader(data_utils.TensorDataset(torch.FloatTensor(feat.values)),
                                            batch_size=64, shuffle=False, drop_last=False)
    pred=np.array([])
    for i, data in enumerate(data_loader, 0):
        score,_= gan.disc.forward(data[0])
        pred= np.concatenate([pred, score.view(-1).detach().numpy()])
    return pred

def trainACGan_generate_fake(df, cond, epochs):
    ohe_dim= list(find_cat_len(df))
    continuous_dim= len(df.columns)- sum(ohe_dim) -1 # -1 pour la colonne target
    gan= ACGanCat(64, continuous_dim, ohe_dim, 0.00001)
    gan.fit(df.drop(cond, axis=1).values, df[cond].values, 64, epochs)
    fake= gan.generate_fake(df[cond].values)
    #fake= gan.generate_fake(real.shape[0])
    fake= pd.DataFrame(fake.cpu().detach().numpy(), columns= df.drop(cond, axis=1).columns)
    fake= pd.concat([fake, df[cond].reset_index(drop=True)], axis=1)
    return fake, gan

def trainWCGan_generate_fake(df, cond, epochs):
    ohe_dim= list(find_cat_len(df))
    continuous_dim= len(df.columns)- sum(ohe_dim) -1 # -1 pour la colonne target
    #gan= WCGanCat(64, continuous_dim, ohe_dim, 0.000001)
    gan= WCGan(64, continuous_dim, 0.000001)
    gan.fit(df.drop(cond, axis=1).values, df[cond].values, 64, epochs)
    fake= gan.generate_fake(df[cond].values)
    #fake= gan.generate_fake(real.shape[0])
    fake= pd.DataFrame(fake.cpu().detach().numpy(), columns= df.drop(cond, axis=1).columns)
    fake= pd.concat([fake, df[cond].reset_index(drop=True)], axis=1)
    return fake, gan

def trainACWGan_generate_fake(df, cond, epochs):
    ohe_dim= list(find_cat_len(df))
    continuous_dim= len(df.columns)- sum(ohe_dim) -1 # -1 pour la colonne target
    gan= IACWGanCat(64, continuous_dim, ohe_dim, 0.00001)
    gan.fit(df.drop(cond, axis=1).values, df[cond].values, 256, epochs)
    fake= gan.generate_fake(df[cond].values)
    #fake= gan.generate_fake(real.shape[0])
    fake= pd.DataFrame(fake.cpu().detach().numpy(), columns= df.drop(cond, axis=1).columns)
    fake= pd.concat([fake, df[cond].reset_index(drop=True)], axis=1)
    return fake, gan

def trainVAE_generate_fake(df, cond,epochs=100):
    vae= CVAE2(df.shape[1], 0.0001)
    vae.fit(df.drop(cond, axis=1).values, df[cond].values,64, epochs)
    fake= vae.generate_fake(df[cond].values)
    fake= pd.DataFrame(fake.detach().cpu().numpy(), columns= df.columns)
    return fake, vae

def collision_attack_ac(training_samples, synth, cond, epochs, int_col):
    synth['collision']= find_overlap(training_samples, synth)
    if synth.loc[synth.collision==1,:].shape[0]==0:
        print("No collision produced, trying approximate match")
        synth= find_aprox_overlap(training_samples, synth.drop('collision', axis=1))
        if synth.loc[synth.collision==1,:].shape[0]==0:
            print("No collision produced, can t run attack")
            return 0,0 #la tete a toto
    synth2, gan= trainACWGan_generate_fake(synth.drop(['collision'], axis=1), cond, epochs)
    for col in int_col:
        synth2[col]= transform_to_ref_values(synth2, training_samples, col, col)
    synth= compute_row_frequency(synth.drop('collision', axis=1))
    synth2= compute_row_frequency(synth2)
    synth2['collision']= find_overlap(synth.drop('freq', axis=1), synth2.drop('freq', axis=1))
    if synth2.loc[synth2.collision==1,:].shape[0]==0:
        print("No collision produced, trying approximate match")
        synth2= find_aprox_overlap(synth.drop(['freq'], axis=1), synth2.drop(['freq'], axis=1))
        if synth2.loc[synth2.collision==1,:].shape[0]==0:
            print("No collision produced, can t run attack")
            return 0,0
    synth['collision']= find_overlap(training_samples, synth.drop('freq', axis=1))
    # train classifier
    rf= RandomForest(synth2.drop('collision', axis=1), synth2['collision'], synth.drop('collision', axis=1))
    synth['score']= rf.train_predict()
    return synth, rf

def collision_attack_wc(training_samples, synth, cond, epochs, int_col):
    synth['collision']= find_overlap(training_samples, synth)
    if synth.loc[synth.collision==1,:].shape[0]==0:
        print("No collision produced, trying approximate match")
        synth= find_aprox_overlap(training_samples, synth.drop('collision', axis=1))
        if synth.loc[synth.collision==1,:].shape[0]==0:
            print("No collision produced, can t run attack")
            return 0,0 #la tete a toto
    print('nb_collisions:', synth.loc[synth.collision==1,:].shape[0])
    synth2, gan= trainWCGan_generate_fake(synth.drop('collision', axis=1), cond, epochs)
    for col in int_col:
        synth2[col]= transform_to_ref_values(synth2, training_samples, col, col)
    synth= compute_row_frequency(synth.drop('collision', axis=1))
    synth2= compute_row_frequency(synth2)
    synth2['collision']= find_overlap(synth.drop('freq', axis=1), synth2.drop('freq', axis=1))
    if synth2.loc[synth2.collision==1,:].shape[0]==0:
        print("No collision produced in synth2, can't run collision attack")
        return 0,0
    synth['collision']= find_overlap(training_samples, synth.drop('freq', axis=1))
    # train classifier
    rf= RandomForest(synth2.drop('collision', axis=1), synth2['collision'], synth.drop('collision', axis=1))
    synth['score']= rf.train_predict()
    return synth, rf

def collision_attack_vae(training_samples, synth, cond, epochs, int_col):
    synth['collision']= find_overlap(training_samples, synth)
    if synth.loc[synth.collision==1,:].shape[0]==0:
        print("No collision produced, trying approximate match")
        synth= find_aprox_overlap(training_samples, synth.drop('collision', axis=1))
        if synth.loc[synth.collision==1,:].shape[0]==0:
            print("No collision produced, can t run attack")
            return 0,0 #la tete a toto
    print('nb_collisions:', synth.loc[synth.collision==1,:].shape[0])

    synth2, gan= trainVAE_generate_fake(synth.drop('collision', axis=1), cond, epochs)
    for col in int_col:
        synth2[col]= transform_to_ref_values(synth2, training_samples, col, col)
    synth= compute_row_frequency(synth.drop('collision', axis=1))
    synth2= compute_row_frequency(synth2)
    synth2['collision']= find_overlap(synth.drop('freq', axis=1), synth2.drop('freq', axis=1))
    if synth2.loc[synth2.collision==1,:].shape[0]==0:
        print("No collision produced in synth2, can't run collision attack")
        return 0,0
    synth['collision']= find_overlap(training_samples, synth.drop('freq', axis=1))
    # train classifier
    rf= RandomForest(synth2.drop('collision', axis=1), synth2['collision'], synth.drop('collision', axis=1))
    synth['score']= rf.train_predict()
    return synth, rf

"""fake= vae.generate_fake(synth[cond].values)
    fake= pd.DataFrame(fake.detach().numpy(), columns= synth.columns)
    #fake= pd.DataFrame(fake.detach().cpu().numpy(), columns= real.columns)
    #int_col= real_x.columns.tolist()[:continuous_dim]
    for col in int_col:
        fake[col]= transform_to_ref_values(fake, synth, col, col)"""
