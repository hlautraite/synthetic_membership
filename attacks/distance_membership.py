import numpy as np
import pandas as pd
from sklearn.neighbors import NearestNeighbors
from sklearn.metrics import confusion_matrix, precision_score, recall_score, f1_score, accuracy_score
from sklearn.decomposition import PCA

def prepare_data(test_x, test_y, test_synth, synth_x, synth_y):
    test_real= pd.concat([test_x, test_y], axis=1)
    test_real['is_member']=1
    test_fake= pd.concat([test_synth, test_y.reset_index(drop=True)], axis=1)
    test_fake['is_member']=0
    test= pd.concat([test_real.reset_index(drop=True), test_fake], axis=0).reset_index(drop=True)
    return test, pd.concat([synth_x, synth_y.reset_index(drop=True)], axis=1)

def find_nearest_neighbor_dist(synth, test, nb_neighbors=1):
    nn= NearestNeighbors(n_neighbors=nb_neighbors)
    nn.fit(synth)
    return nn.kneighbors(test)[0]

def count_in_eps_ball(synt, test, eps):
    nn= NearestNeighbors(n_neighbors=nb_neighbors)
    nn.fit(synth)


def evaluate_attack(test, score, percentile= 0.5):
    test['pred']= score
    threshold= test.pred.quantile(0.5)


def gan_leak_attack(training_samples, non_training_samples, synth,pct_real):
    training_samples['is_member']=1
    non_training_samples['is_member']=0
    test= pd.concat([training_samples.reset_index(drop=True), non_training_samples.reset_index(drop=True)], axis=0)
    if test.shape[1]>=20:
        pca= PCA(n_components=10)
        pca.fit(test.drop('is_member', axis=1))
        test_pca= pca.transform(test.drop('is_member', axis=1))
        synth_pca= pca.transform(synth)
        test['score']= find_nearest_neighbor_dist(synth_pca, test_pca)
    else:
        test['score']= find_nearest_neighbor_dist(synth, test.drop('is_member', axis=1))
    threshold= test['score'].quantile(pct_real)
    test['pred']= np.where(test.score <= threshold,1,0)
    return test

def monte_carlo_attack(training_samples, non_training_samples, synth,pct_real):
    training_samples['is_member']=1
    non_training_samples['is_member']=0
    test= pd.concat([training_samples.reset_index(drop=True), non_training_samples.reset_index(drop=True)], axis=0)
    if test.shape[1]>=20:
        pca= PCA(n_components=10)
        pca.fit(test.drop('is_member', axis=1))
        test_pca= pca.transform(test.drop('is_member', axis=1))
        synth_pca= pca.transform(synth)
        eps= np.median(find_nearest_neighbor_dist(synth_pca, test_pca))
        nn= NearestNeighbors(radius=eps, n_jobs=-1)
        nn.fit(synth_pca)
        rad_neb= nn.radius_neighbors(test_pca)[0]
    else:
        eps= np.median(find_nearest_neighbor_dist(synth, test.drop('is_member', axis=1)))
        nn= NearestNeighbors(radius=eps, n_jobs=-1)
        nn.fit(synth)
        rad_neb= nn.radius_neighbors(test.drop('is_member', axis=1))[0]
    test['score']= [len(i)/test.shape[0] for i in rad_neb]
    threshold= test['score'].quantile(pct_real)
    test['pred']= np.where(test.score >= threshold,1,0)
    return test

"""
test=gan_leak_attack(real.sample(n= test_membersip.shape[0], random_state=42),test_membersip, pd.concat([fake, real_y.reset_index(drop=True)], axis=1),.5)


print('confusion matrix:',confusion_matrix(test.is_member, test.pred))
print('precision',precision_score(test.is_member, test.pred))
print('recall',recall_score(test.is_member, test.pred))
print('f1:',f1_score(test.is_member, test.pred))
print('accuracy', accuracy_score(test.is_member, test.pred))
"""
