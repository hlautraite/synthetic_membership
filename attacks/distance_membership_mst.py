import pandas as pd
from sklearn.metrics import confusion_matrix, precision_score, recall_score, f1_score, accuracy_score
import glob
from attacks.distance_membership import *
import matplotlib.pyplot as plt
from functools import reduce

def print_membership_results(test):
  return precision_score(test.is_member, test.pred)

train= pd.read_csv('D:/data/datasets/datasets/employment/Employment_ori.csv').drop(columns=['AGEP'])
non_train= pd.read_csv('D:/data/datasets/datasets/employment/Employment_unused.csv').drop(columns=['AGEP'])

def run_attacks(train, non_train, synth_path_list):
    dataset_list=[]
    eps_list=[]
    generation_list=[]
    gl_precision_avg_50=[]
    mc_precision_avg_50= []
    gl_precision_avg_1=[]
    mc_precision_avg_1= []
    for p in synth_path_list:
        synth_path= glob.glob(p)
        print(p)
        synth_list=[]
        for p in synth_path:
            synth_list.append(pd.read_csv(p))
        gan_leak_50, gan_leak_1, mc_50, mc_1= [],[],[],[]
        print(p)
        eps= ''
        eps= int(eps.join([i for i in p.split('/')[6] if i.isdigit()]))
        generation= p.split('/')[5]
        dataset= p.split('/')[4]
        if dataset=='employment' and generation=='MST' and eps== 1:
            for i in range(len(synth_list)):
                synth_list[i]= synth_list[i].drop('AGEP', axis=1)
        if dataset=='employment' and generation=='privbayes':
            for i in range(len(synth_list)):
                synth_list[i]= synth_list[i].drop('AGEP', axis=1)
        fake= reduce(lambda df1, df2: pd.concat([df1,df2], axis=0,  ignore_index=True), synth_list)
        #for fake in synth_list:
        test=gan_leak_attack(train,non_train, fake,.5)
        gan_leak_50.append(print_membership_results(test))
        threshold= test.score.quantile(.01)
        test['pred']= np.where(test.score<= threshold,1,0)
        gan_leak_1.append(print_membership_results(test))
        #vizualize_membership(test, 'tsne', 2)
        test_mc=monte_carlo_attack(train,non_train, fake,.5)
        mc_50.append(print_membership_results(test_mc))
        threshold= test_mc.score.quantile(.99)
        test_mc['pred']= np.where(test_mc.score>= threshold,1,0)
        mc_1.append(print_membership_results(test_mc))
        #vizualize_membership(test, 'tsne', 2)
        dataset_list.append(dataset)
        eps_list.append(eps)
        generation_list.append(generation)
        gl_precision_avg_50.append(np.mean(gan_leak_50))
        gl_precision_avg_1.append(np.mean(gan_leak_1))
        mc_precision_avg_50.append(np.mean(mc_50))
        mc_precision_avg_1.append(np.mean(mc_1))
    df= pd.DataFrame({'dataset': dataset_list,
    'generation_method': generation_list,
    'eps': eps_list,
    'gan_leak_50': gl_precision_avg_50,
    'gan_leak_1': gl_precision_avg_1,
    'mc_50': mc_precision_avg_50,
    'mc_1': mc_precision_avg_1})
    print(df)
    df.to_csv('D:/Phd/synthetic_membership/distance_membership_employment_MST10.csv', index=False)

if __name__=='__main__':
    run_attacks(train, non_train, ['D:/data/datasets/datasets/employment/MST/esp100/synth/*.csv',
    'D:/data/datasets/datasets/employment/MST/esp10/synth/*.csv',
    'D:/data/datasets/datasets/employment/MST/esp1/synth/*.csv',
    'D:/data/datasets/datasets/employment/MST fair/esp100/synth/*.csv',
    'D:/data/datasets/datasets/employment/MST fair/esp10/synth/*.csv',
    'D:/data/datasets/datasets/employment/MST fair/esp1/synth/*.csv',
    'D:/data/datasets/datasets/employment/MWEM/esp100/synth/*.csv',
    'D:/data/datasets/datasets/employment/MWEM/esp10/synth/*.csv',
    'D:/data/datasets/datasets/employment/MWEM/esp1/synth/*.csv',
    'D:/data/datasets/datasets/employment/privbayes/esp10/*.csv',
    ])

#        plt.plot(gan_leak_50, label= 'gan_leak_50')
#        plt.plot(gan_leak_1, label= 'gan_leak_1')
#        plt.title("Gan Leak")
#        plt.legend()
#        plt.show()
#        print('gan leak avg precision 50:', np.mean(gan_leak_50))
#        print('gan leak avg precision 1:', np.mean(gan_leak_1))
#
#        plt.plot(mc_50, label= 'mc_50')
#        plt.plot(mc_1, label= 'mc_1')
#        plt.title("MC")
#        plt.legend()
#        plt.show()
#        print('mc avg precision 50:', np.mean(gan_leak_50))
#        print('mc avg precision 1:', np.mean(gan_leak_1))