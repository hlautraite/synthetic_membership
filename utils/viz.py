from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

def tsne_dim_reduc(df, nb_dim=2):
    tsne= TSNE(n_components= nb_dim, init='pca', learning_rate='auto')
    return tsne.fit_transform(df)

def pca_dim_reduc(df, nb_dim=2):
    pca= PCA(n_components=nb_dim)
    pca.fit(df)
    return pca.transform(df)

def classification_label(r, label='is_member'):
    if r['pred']== r[label] and r['pred']==0:
        return 'True Negative'
    elif r['pred']== r[label] and r['pred']==1:
        return 'True Positive'
    elif r['pred']==0 and r[label]==1:
        return 'False Negative'
    else:
        return 'False Positive'

def add_classification_eval(df):
    df['classification eval']= df.apply(lambda r: classification_label(r), axis=1)
    return df

def vizualize_membership(df, dim_reduc, nb_dim):
    if dim_reduc=='tsne':
        df_reduc= tsne_dim_reduc(df.drop(columns=['pred', 'score', 'is_member']), nb_dim)
    elif dim_reduc== 'pca':
        df_reduc= pca_dim_reduc(df.drop(columns=['pred', 'score', 'is_member']), nb_dim)
    elif dim_reduc== 'pca+tsne':
        df_reduc= pca_dim_reduc(df, 20)
        df_reduc= tsne_dim_reduc(df.drop(columns=['pred', 'score', 'is_member']), nb_dim)
    df= add_classification_eval(df)
    df_viz= pd.concat([df.reset_index(drop=True)['classification eval'], pd.DataFrame(df_reduc)], axis=1)
    if nb_dim==2:
        sns.relplot(x=0, y=1,data=df_viz, hue= 'classification eval', alpha=.1)
        plt.show()
    if nb_dim==3:
        plot_3d(df_viz)

def plot_3d(df):
    fig = plt.figure(figsize=(12, 12))
    ax = fig.add_subplot(projection='3d')
    ax.scatter(df.loc[df['classification eval']== 'True Positive',0], df.loc[df['classification eval']== 'True Positive',1], df.loc[df['classification eval']== 'True Positive',2], label='True Positive', alpha=0.2)
    ax.scatter(df.loc[df['classification eval']== 'False Positive',0], df.loc[df['classification eval']== 'False Positive',1], df.loc[df['classification eval']== 'False Positive',2], label='False Positive', alpha=0.2)
    ax.scatter(df.loc[df['classification eval']== 'True Negative',0], df.loc[df['classification eval']== 'True Negative',1], df.loc[df['classification eval']== 'True Negative',2], label ='True Negative', alpha=0.2)
    ax.scatter(df.loc[df['classification eval']== 'False Negative',0], df.loc[df['classification eval']== 'False Negative',1], df.loc[df['classification eval']== 'False Negative',2], label='False Negative', alpha=0.2)
    plt.legend()
    plt.show()