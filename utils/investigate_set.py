import pandas as pd
import numpy as np
from sklearn.neighbors import NearestNeighbors
from sklearn.decomposition import PCA
from sklearn.ensemble import IsolationForest
from sklearn.preprocessing import OneHotEncoder, StandardScaler
import matplotlib.pyplot as plt


def find_nearest_neighbor_dist(df, nb_neighbors):
    nn= NearestNeighbors(n_neighbors=nb_neighbors)
    nn.fit(df)
    return np.mean(nn.kneighbors(df)[0], axis=1)

def prepare_data(df, ohe_col,pca=True, scale=True):
    if len(ohe_col)>1:
        ohe= OneHotEncoder(sparse=False)
        df_ohe= ohe.fit_transform(df[ohe_col])
        df= pd.concat([df[[col for col in df.columns if col not in ohe_col]].reset_index(drop=True),
                    pd.DataFrame(df_ohe, columns= ohe.get_feature_names())], axis=1)
    if pca:
        pca= PCA(n_components=10)
        pca.fit(df)
        df= pca.transform(df)
    if scale:
        sc= StandardScaler()
        df=sc.fit_transform(df)
    return df

def dist_analysis(df, collision_col, nb_neighbors=5, ohe_col=[],pca=True, scale=True):
    df_prep= prepare_data(df.drop(collision_col, axis=1), ohe_col,pca, scale)
    df['dist_neighbors']= find_nearest_neighbor_dist(df_prep, nb_neighbors)
    print('collision avg dist:', df.loc[df[collision_col]==1, 'dist_neighbors'].mean()
        , 'collision median_dist:', df.loc[df[collision_col]==1, 'dist_neighbors'].median())
    print('avg dist unique:', df.loc[df[collision_col]==0, 'dist_neighbors'].mean()
        , 'unique median_dist:', df.loc[df[collision_col]==0, 'dist_neighbors'].median())
    plt.hist(df.loc[df[collision_col]==1, 'dist_neighbors'], label='collision', density=True, alpha=0.5)
    plt.hist(df.loc[df[collision_col]==0, 'dist_neighbors'], label='unique', density=True, alpha=0.5)
    plt.legend()
    plt.show()

def isolation_forest_analysis(df, collision_col, nb_trees=100):
    iso_f= IsolationForest(n_estimators=nb_trees, n_jobs=-1, random_state=42)
    iso_f.fit(df)
    df['isolation_score']= iso_f.score_samples(df)
    print('collision avg isolation score:', df.loc[df[collision_col]==1, 'isolation_score'].mean()
        , 'collision median isolation score:', df.loc[df[collision_col]==1, 'isolation_score'].median())
    print('avg isolation score unique:', df.loc[df[collision_col]==0, 'isolation_score'].mean()
        , 'unique median isolation score:', df.loc[df[collision_col]==0, 'isolation_score'].median())
    plt.hist(df.loc[df[collision_col]==1, 'isolation_score'], label='collision', density=True, alpha=0.5)
    plt.hist(df.loc[df[collision_col]==0, 'isolation_score'], label='unique', density=True, alpha=0.5)
    plt.legend()
    plt.show()

    
