import pandas as pd
import numpy as np
from sklearn.neighbors import NearestNeighbors


def transform_to_ref_values(df_col, df_ref, col, ref):
    nn= NearestNeighbors(n_neighbors=1, n_jobs=-1)
    df_ref= df_ref.loc[:,[ref]].drop_duplicates().reset_index(drop=True)
    nn.fit(df_ref[[ref]])
    ref_pos= nn.kneighbors(df_col[[col]])
    ref_col= []
    for pos in ref_pos[1]:
        ref_col.append(df_ref[ref][pos[0]])
    return ref_col
