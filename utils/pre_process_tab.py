import pandas as pd 
import numpy as np 
import joblib
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.model_selection import train_test_split
import re
from collections import Counter

def load_data(data_path, drop_col):
    df= pd.read_csv(data_path)
    df= df.drop(columns= drop_col)
    df= df.dropna()
    return df

def split_train_test(df, y):
    df_y= df[y]
    df_x= df.drop(columns= y)
    train_x, test_x, train_y, test_y= train_test_split(df_x, df_y, test_size=0.2, stratify=df_y, random_state=42)
    return train_x, test_x, train_y, test_y

def get_num_col(df):
    return [col for col in df.columns if df[col].dtypes !='object']

def scale_col(train_x, test_x, sc_col, save_path):
    sc= StandardScaler()
    train_sc= train_x.drop(columns=[col for col in train_x.columns if col not in sc_col])
    test_sc= test_x.drop(columns= [col for col in test_x.columns if col not in sc_col])
    train_sc[sc_col]= sc.fit_transform(train_x[sc_col])
    test_sc[sc_col]= sc.transform(test_x[sc_col])
    if save_path:
        joblib.dump(sc, save_path)
    return train_sc, test_sc

def ohe(train_x, test_x, ohe_col):
    ohe= OneHotEncoder(sparse=False, handle_unknown='ignore')
    train_ohe= ohe.fit_transform(train_x[ohe_col])
    test_ohe= ohe.transform(test_x[ohe_col])
    return pd.DataFrame(train_ohe, columns= ohe.get_feature_names()), pd.DataFrame(test_ohe, columns= ohe.get_feature_names())

def load_preprocess(data_path, drop_col, y_col, save_path=None):
    df= load_data(data_path, drop_col)
    train_x, test_x, train_y, test_y= split_train_test(df, y_col)
    num_col= get_num_col(df)
    cat_col= [col for col in df.columns if col not in num_col]
    try: 
        num_col.remove(y_col)
    except:
        print('can t remove y from numcol')
        pass
    if len(num_col)>0:
        train_sc, test_sc= scale_col(train_x, test_x, num_col, save_path)
    if len(cat_col)>0:
        train_ohe, test_ohe= ohe(train_x, test_x, cat_col)
    if 'train_sc' in locals() and 'train_ohe' in locals():
        train_x= pd.concat([train_sc.reset_index(drop=True), train_ohe.reset_index(drop=True)], axis=1)
        test_x= pd.concat([test_sc.reset_index(drop=True), test_ohe.reset_index(drop=True)], axis=1)
    elif not 'train_sc' in locals() and 'train_ohe' in locals():
        train_x= train_ohe.reset_index(drop=True)
        test_x= test_ohe.reset_index(drop=True)
    elif not 'train_ohe' in locals() and 'train_sc' in locals():
        train_x, test_x= train_sc.reset_index(drop=True), test_sc.reset_index(drop=True)
    return train_x, test_x, train_y.reset_index(drop=True), test_y.reset_index(drop=True)

def find_cat_len(df):
    reg= re.compile(r'x\d+')
    cat_col= [col for col in df.columns if reg.match(col)]
    rad_list= [col.split('_')[0] for col in cat_col]
    return Counter(rad_list).values()



